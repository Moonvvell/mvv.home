<?php
/**
 * Created by PhpStorm.
 * User: moonvvell
 * Date: 15.10.2017
 * Time: 18:20
 */

namespace app\components;

use Yii;
use \yii\helpers\Html;
use yii\widgets\ActiveForm;

class TableActionsComponent
{
    public static $showButton = 'view';
    public static $editButton = 'update';
    public static $deleteButton = 'delete';
    public static $cloneButton = 'clone';
    public static $allButtons = ['view', 'update', 'delete', 'clone'];
    public static $icons = [
        'view' => 'table-item-view fa fa-eye',
        'update' => 'table-item-update fa fa-pencil',
        'delete' => 'table-item-update fa fa-trash-o',
        'clone' => 'table-item-clone fa fa-plus-square-o'
    ];
    public static $buttonsClass = [
        'view' => 'btn btn-primary btn-xs',
        'update' => 'btn btn-info btn-xs',
        'delete' => 'btn btn-danger btn-xs delete-object',
        'clone' => 'btn btn-warning btn-xs clone-object'
    ];

    /**
     * @param array $buttons
     * @param int $id
     * @param string $controllerName
     * @return string
     */
    public static function getActionsButtons($buttons, $id, $controllerName)
    {
        $html = Html::beginForm(\Yii::$app->getUrlManager()->createUrl([$controllerName . '/delete', 'id' => $id]), 'post', ['id' => 'deleteObject']);
        foreach ($buttons as $button) {
            $attributes = ['class' => self::$buttonsClass[$button]];

            if ($button === self::$deleteButton) {
                $attributes['data-confirm'] = \Yii::t('app', 'Do you really want to delete this element?');
            }
            $icon = Html::tag('i', '', ['class' => self::$icons[$button]]);
            $link = Html::a(
                $icon,
                \Yii::$app->getUrlManager()->createUrl([$controllerName . '/' . $button, 'id' => $id]),
                $attributes);
            $html .= $link;
        }
        $html .= Html::endForm();
        return $html;
    }

    /**
     * @param array $buttons
     * @param int $id
     * @param string $controllerName
     * @return array
     */
    public static function getActionButtonsForGridView($buttons, $controllerName)
    {
        $template = '';
        foreach ($buttons as $button) {
            $template .= '{' . $button . '}';
        }
        return [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('app', 'Actions'),
            'contentOptions' => ['class' => 'table-actions'],
            'template' => $template,
            'buttons' => self::getButtonsActions($buttons)
        ];
    }

    private static function getButtonsActions($buttons)
    {
        $return = [];
        foreach ($buttons as $button) {
            $return[$button] = function ($url, $model) use ($button) {
                $icon = Html::tag('i', '', ['class' => self::$icons[$button]]);
                $attributes = [
                    'class' => self::$buttonsClass[$button],
                    'title' => Yii::t('app', ucfirst($button))
                ];
                if ($button === self::$deleteButton) {
                    $attributes['data-confirm'] = \Yii::t('app', 'Do you really want to delete this element?');
                    $attributes['data-method'] = 'post';
                    $attributes['data-pjax'] = '0';

                }
                return Html::a($icon, $url, $attributes);
            };
        }
        return $return;
    }
}
