<?php
/**
 * Created by PhpStorm.
 * User: ihor.kyryliuk
 * Date: 11.07.2018
 * Time: 14:19
 */

namespace app\components\menu;


class MenuSlugs
{
    public const HOME = 'home';
    public const JAPAN_LEARNING = 'japan_learning';
    public const KANA_LEARNING = 'kana_learning';
    public const INSECTS_COLLECTION = 'insects_collection';
    public const INSECTS_LABELS = 'insects_labels';
    public const BIRDS = 'module_birds';
    public const CONTACTS = 'contacts';
    public const CONTACT_LIST = 'contact_list';
    public const CONTACT_GROUPS = 'contact_groups';
    public const CONTACT_CONTACTS = 'contact_contacts';
    public const USERS = 'users';
    public const FILE_LIST = 'file_list';
    public const FILE_ROLES = 'file_roles';
    public const LOGS = 'logs';
}
