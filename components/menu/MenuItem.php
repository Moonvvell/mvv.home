<?php
/**
 *
 * Created by PhpStorm.
 * User: ihor.kyryliuk
 * Date: 11.07.2018
 * Time: 14:32
 */

namespace app\components\menu;

class MenuItem
{
    public $menuItems = [];
    public $name;
    public $link;
    public $icon;
    public $slug;
    public $active;


    private function __construct($name, $link, $icon, $slug)
    {
        $this->name = $name;
        $this->link = $link;
        $this->icon = $icon;
        $this->slug = $slug;
    }

    private function getActiveClass(): string
    {
        return $this->active ? 'active' : '';
    }

    private function renderSubItems(): void
    {
        foreach ($this->menuItems as $subMenuItem) {
            $subMenuItem->render();
        }
    }

    public static function create($name, $icon, $link = null, $slug = null): MenuItem
    {
        return new self($name, $link, $icon, $slug);
    }

    public function render(): void
    {
        if (!empty($this->menuItems)) {
            $activeClass = $this->getActiveClass();
            echo "<li class='$activeClass'><a href='#'><i class='fa $this->icon'></i><span class='nav-label'>$this->name</span><span class='fa arrow'></span></a><ul class='nav nav-second-level'>";
            $this->renderSubItems();
            echo '</ul></li>';
        } else {
            $activeClass = $this->getActiveClass();
            echo "<li class='$activeClass'><a href='$this->link'><i class='fa $this->icon'></i> <span class='nav-label'>$this->name</span></a></li>";
        }
    }

    public function hasSubItems(): bool
    {
        return !empty($this->menuItems);
    }
}
