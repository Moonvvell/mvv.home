<?php
/**
 * Created by PhpStorm.
 * User: ihor.kyryliuk
 * Date: 11.07.2018
 * Time: 14:32
 */

namespace app\components\menu;

class Menu
{
    /** @var MenuItem[] */
    public static $menuItems = [];

    public static function addMenuItem(MenuItem $menuItem): void
    {
        self::$menuItems[] = $menuItem;
    }

    public static function render(): void
    {
        foreach (self::$menuItems as $menuItem) {
            $menuItem->render();
        }
    }
}
