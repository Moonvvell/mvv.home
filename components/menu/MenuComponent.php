<?php
/**
 * Created by PhpStorm.
 * User: ihor.kyryliuk
 * Date: 13.06.2018
 * Time: 16:09
 */

namespace app\components\menu;

use Yii;

class MenuComponent
{
    /** @var Menu $menu */
    private static $menu;

    /**
     * Create instance of menu or return existing instance
     * @return Menu
     */
    public static function GetMenu(): Menu
    {
        if (self::$menu === null) {
            self::createMenu();
        }
        return self::$menu;
    }

    public static function setActiveMenu(string $slug): void
    {
        if (self::$menu === null) {
            self::createMenu();
        }
        $menu = self::$menu;
        foreach ($menu::$menuItems as $key => $menuItem) {
            if ($menuItem instanceof MenuItem && $menuItem->slug === $slug) {
                $menuItem->active = true;
                self::$menu::$menuItems[$key] = $menuItem;
            }
            if ($menuItem->hasSubItems()) {
                /** @var MenuItem $item */
                foreach ($menuItem->menuItems as $item) {
                    if ($item->slug === $slug) {
                        $menuItem->active = true;
                        $item->active = true;
                        self::$menu::$menuItems[$key] = $menuItem;
                    }
                }
            }
        }
    }

    private static function createMenu(): void
    {
        self::$menu = new Menu();
        self::$menu::addMenuItem(MenuItem::create(Yii::t('app', 'Main page'), MenuIcons::HOME, Yii::$app->homeUrl, MenuSlugs::HOME));

        $kanaLearningGroup = MenuItem::create(Yii::t('app', 'Japan Learning'), MenuIcons::JAPAN_LEARNING);
        $kanaLearningGroup->menuItems[] = MenuItem::create(Yii::t('app', 'Kana learning'), MenuIcons::EMPTY, '/kana-learn', MenuSlugs::KANA_LEARNING);
        self::$menu::addMenuItem($kanaLearningGroup);


        self::$menu::addMenuItem(MenuItem::create(Yii::t('app', 'Insects collection'), MenuIcons::INSECTS_COLLECTION, '/insects-collection', MenuSlugs::INSECTS_COLLECTION));
        self::$menu::addMenuItem(MenuItem::create(Yii::t('app', 'Insects labels'), MenuIcons::INSECTS_COLLECTION, '/insects-labels', MenuSlugs::INSECTS_LABELS));
        self::$menu::addMenuItem(MenuItem::create(Yii::t('app', 'Birds'), MenuIcons::BIRDS, '/module-birds', MenuSlugs::BIRDS));


        $filesGroup = MenuItem::create(Yii::t('app', 'Files'), MenuIcons::FILE_LIST);
        $filesGroup->menuItems[] = MenuItem::create(Yii::t('app', 'File list'), MenuIcons::EMPTY, '/file', MenuSlugs::FILE_LIST);
        $filesGroup->menuItems[] = MenuItem::create(Yii::t('app', 'File roles'), MenuIcons::EMPTY, '/file', MenuSlugs::FILE_ROLES);
        self::$menu::addMenuItem($filesGroup);
    }
}
