<?php
/**
 * Created by PhpStorm.
 * User: ihor.kyryliuk
 * Date: 11.07.2018
 * Time: 14:19
 */

namespace app\components\menu;


class MenuIcons
{
    public const EMPTY = '';
    public const HOME = 'fa-home';
    public const JAPAN_LEARNING = 'fa-male';
    public const INSECTS_COLLECTION = 'fa-bug';
    public const BIRDS = 'fa-tree';
    public const CONTACTS = 'fa-male';
    public const USERS = 'fa-users';
    public const FILE_LIST = 'fa-file';
    public const LOGS = 'fa-cog';
}
