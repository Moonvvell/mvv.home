#default parameters, can be overriden by script invocation in command line
param(
    [switch]$parametered = $false,
    [switch]$force = $false,
    [string]$dbuser = "root",
    [string]$dbpass = "root",
	[string]$dbname = "moonvvell.home",
	[string]$dbhost = "localhost"
)

function Replace-In-File($filePath, $from, $to)
{
    $content = [System.IO.File]::ReadAllText($filePath).Replace($from, $to)
    [System.IO.File]::WriteAllText($filePath, $content)
}

if ($parametered -eq $false){
    $dbuser = Read-Host 'Enter database username [default=root]'
    if ($dbuser -eq ''){
        $dbuser = 'root'
    }
    $dbpass = Read-Host 'Enter database password [default=]'
    $dbname = Read-Host 'Enter database name [default=moonvvell.home]'
    if ($dbname -eq ''){
        $dbname = 'rightinformation.administration.system'
    }
    $dbhost = Read-Host 'Enter database host [default=localhost]'
    if ($dbhost -eq ''){
        $dbhost = 'localhost'
    }
}



$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

$items = Get-Childitem -recurse -Path $scriptPath -Filter '*.sample'

foreach($item in $items)
{
	$oldFile = $item.FullName
	$newFile = $item.FullName.replace('.sample','')
    #copying new file only if it not exists or force mode is on - then everything is replaced
	if(-Not (Test-Path -Path $newFile) -or $force -eq $true)
	{
    "Copying $oldFile $newFile"
	Copy-Item -Path $oldFile -Destination $newFile
    Replace-In-File $newFile '%dbuser%' "$dbuser"
	Replace-In-File $newFile '%dbpass%' "$dbpass"
    Replace-In-File $newFile '%dbname%' "$dbname"
	Replace-In-File $newFile '%dbhost%' "$dbhost"
	}
}



