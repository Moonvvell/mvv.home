<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 12.02.2018
 * Time: 19:00
 */
?>
<script>
    window.kanaInitData = <?= json_encode($arrayData) ?>;
</script>
<section id="kanaLearnSection">
    <section class="info">
        <div class="info-block">
            <div class="info-block__color hiragana"></div>
            <div class="info-block__text"> <?= Yii::t('japan-l', 'Hiragana color') ?></div>
        </div>
        <div class="info-block">
            <div class="info-block__color katakana"></div>
            <div class="info-block__text"> <?= Yii::t('japan-l', 'Katakana color') ?></div>
        </div>
    </section>
    <section class="actions">
        <section class="actions__block actions__block--main">
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowFullList" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Show full list') ?></span>
            </label>
            <label class="custom-control custom-text-input" v-if="!showFullList">
                <input type="text" v-model="startLearnItems" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Enter number of shown symbols') ?></span>
            </label>
            <button @click="toggleLearning()" v-if="!learning && !showFullList"><?= Yii::t('japan-l', 'Start learning') ?></button>
            <button @click="toggleLearning()" v-if="learning && !showFullList"><?= Yii::t('japan-l', 'Stop learning') ?></button>
            <button @click="generateItems()" v-if="!showFullList"><?= Yii::t('japan-l', 'Generate items to learn') ?></button>
        </section>
        <section class="actions__block--hiragana actions__block">
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowHiragana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Hiragana') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowHiraganaBase" :disabled="!canShowHiragana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Base') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowHiraganaYoon" :disabled="!canShowHiragana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Yoon') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowHiraganaHanDakuten" :disabled="!canShowHiragana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', '(Han)dakuten') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowHiraganaYoonHanDakuten" :disabled="!canShowHiragana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Yoon (han)dakuten') ?></span>
            </label>
        </section>
        <section class="actions__block--katakana actions__block">
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowKatakana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Katakana') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowKatakanaBase" :disabled="!canShowKatakana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Base') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowKatakanaYoon" :disabled="!canShowKatakana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Yoon') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowKatakanaHanDakuten" :disabled="!canShowKatakana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', '(Han)dakuten') ?></span>
            </label>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" v-model="canShowKatakanaYoonHanDakuten" :disabled="!canShowKatakana" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label"><?= Yii::t('japan-l', 'Yoon (han)dakuten') ?></span>
            </label>
        </section>
    </section>
    <section class="kana-learn-section">
        <template v-for="(item, idx) in getKanaData()">
            <div :data-type="item.type" :data-subtype="item.subType" class="kana-learn-item" v-if="item" @click="showSymbolIfHidden($event)">
                <div class="kana-learn-item__name">
                    {{item.name}}
                </div>
                <div class="kana-learn-item__symbol" :class="learning ? 'hide-symbol' : ''">
                    {{item.symbol}}
                </div>
            </div>
        </template>
    </section>
</section>

<!--<section >-->
<!--    --><?php ///** @var \app\enumDataHelpers\JapanLearning\KanaItem $item */
//    foreach ($kanaData as $item): ?>

<!--    --><?php //endforeach; ?>
<!--</section>-->

