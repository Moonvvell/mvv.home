<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ModuleContactsGroups */

$this->title = Yii::t('app', 'Create Module Contacts Groups');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Module Contacts Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-contacts-groups-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
