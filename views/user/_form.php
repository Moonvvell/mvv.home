<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $avatarForm app\models\UploadForm */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-page-container">
    <div class="user-page-container__form w-50 helper-overflow__hidden helper-padding__LR--20">
        <?php $form = ActiveForm::begin(['action' => 'update?id=' . $model->id, 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="common-input helper-padding__LR--20">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label('Логін') ?>
        </div>
        <div class="common-input helper-padding__LR--20">
            <?= $form->field($model, 'color', ['template' => '{label} {input}'])->input('color', ['class' => 'colorpicker-input'])->label('Колір') ?>
        </div>
        <div class="common-input helper-padding__LR--20">
            <?= $form->field($model, 'user_avatar_id')->textInput() ?>
        </div>
        <div class="helper-padding__LR--20">
            <?= Html::submitButton(Yii::t('app', 'Зберегти'), ['class' => 'common-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="user-page-container__ava-upload w-50 helper-overflow__hidden helper-padding__LR--20">
        <?php Pjax::begin(['enablePushState' => false]) ?>

        <?php if (isset($photoData) && !empty($photoData)): ?>
            <div class="user-page-container__ava-container">
                <div class="user-page-container__ava"
                     style="background: url('/<?php echo $photoData['filePath']; ?>')"></div>
            </div>
        <?php endif; ?>


        <?php $form = ActiveForm::begin(['action' => 'upload', 'options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true]]) ?>

        <div class="user-page-container__ava-upload-buttons">
            <?= $field = $form->field($avatarForm, 'imageFile')->fileInput(['class' => 'mvv-file-input'])->label(false);
            $field->validateOnBlur = false;
            $field->validateOnChange = false;
            $field->validateOnType = false;
            ?>
            <?= Html::submitButton(Yii::t('app', 'Завантажити'), ['class' => 'common-button common-button__border-right']) ?>
        </div>

        <?php ActiveForm::end() ?>
        <script>    $(".mvv-file-input").mvvFileInput();</script>
        <?php Pjax::end(); ?>
    </div>
</div>

