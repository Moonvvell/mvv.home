<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $avatarForm app\models\UploadForm */
/* @var $photoData array */
$model = Yii::$app->user->getIdentity(true);
$this->title = "Моя адмін панель";
?>
<div class="user-panel">
    <?= $this->render('_form', [
        'model' => $model,
        'avatarForm' => $avatarForm,
        'photoData' => $photoData
    ]) ?>

</div>
