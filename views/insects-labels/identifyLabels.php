<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 23.09.2018
 * Time: 18:02
 */ ?>
<div class="identify-labels">
    <?php /** @var \app\models\insectslabels\InsectIdentifyLabel $labelModel */
    foreach ($data as $labelModel):?>
        <?php for ($i = 0; $i < $labelModel->labelsCount; $i++): ?>
            <div class="identify-label">
                <div class="header">
                    <div class="first-line">
                        <?= $labelModel->firstLineText ?>
                    </div>
                </div>
                <div class="footer">
                    <div class="identify-date">
                        <?= $labelModel->date ?>
                    </div>
                    <div class="gender">
                        <?= $labelModel->gender ?>
                    </div>
                    <div class="user-column">
                        <div class="leg">Det.</div>
                        <div class="leg-name">
                            <?= $labelModel->det ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    <?php endforeach; ?>
</div>
