<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 23.09.2018
 * Time: 17:13
 */
?>
<div class="labels-lists">
    <div class="labels-list">
        <div class="title">Geo labels</div>
        <?php foreach ($geoFileNames as $fileName): ?>
            <div class="geo-file-names">
                <?= \yii\helpers\Html::a($fileName, ['insects-labels/geo-labels', 'filename' => $fileName]) ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="labels-list">
        <div class="title">Identify labels</div>
        <?php foreach ($identifyNames as $fileName): ?>
            <div class="geo-file-names">
                <?= \yii\helpers\Html::a($fileName, ['insects-labels/identify-labels', 'filename' => $fileName]) ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<div id="insectsLabels"></div>
