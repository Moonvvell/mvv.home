<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 20.01.2019
 * Time: 19:39
 */
?>
<div class="insect-labels">
    <?php /** @var \app\models\insectslabels\InsectLabel $labelsData */
    foreach ($data as $labelsData): ?>
        <div class="labels-block">
            <div class="geo-label">
                <div class="header">
                    <div class="first-line">
                        <?= $labelsData->geoLabel->firstLineText ?>
                    </div>
                    <div class="second-line">
                        <?= $labelsData->geoLabel->secondLineText ?>
                    </div>
                </div>
                <div class="footer">
                    <div class="date-column">
                        <div class="catch-date">
                            <?= $labelsData->geoLabel->date ?>
                        </div>
                        <div class="coordinates">
                            (<?= $labelsData->geoLabel->coordinates ?>)
                        </div>
                    </div>
                    <div class="user-column">
                        <div class="leg">Leg.</div>
                        <div class="leg-name">
                            <?= $labelsData->geoLabel->leg ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="identify-label">
                <div class="header">
                    <div class="first-line">
                        <?= $labelsData->identLabel->firstLineText ?>
                    </div>
                </div>
                <div class="footer">
                    <div class="identify-date">
                        <?= $labelsData->identLabel->date !== '' ? $labelsData->identLabel->date : '&nbsp;' ?>
                    </div>
                    <div class="gender">
                        <?= $labelsData->identLabel->gender ?>
                    </div>
                    <div class="user-column">
                        <div class="leg">Det.</div>
                        <div class="leg-name">
                            <?= $labelsData->identLabel->det ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
