<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 23.09.2018
 * Time: 18:02
 */ ?>
<div class="geo-labels">
    <?php /** @var \app\models\insectslabels\InsectGeoLabel $labelModel */
    foreach ($data as $labelModel):?>
        <?php for ($i = 0; $i < $labelModel->labelsCount; $i++): ?>
            <div class="geo-label">
                <div class="header">
                    <div class="first-line">
                        <?= $labelModel->firstLineText ?>
                    </div>
                    <div class="second-line">
                        <?= $labelModel->secondLineText ?>
                    </div>
                </div>
                <div class="footer">
                    <div class="date-column">
                        <div class="catch-date">
                            <?= $labelModel->date ?>
                        </div>
                        <div class="coordinates">
                            (<?= $labelModel->coordinates ?>)
                        </div>
                    </div>
                    <div class="user-column">
                        <div class="leg">Leg.</div>
                        <div class="leg-name">
                            <?= $labelModel->leg ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    <?php endforeach; ?>
</div>
