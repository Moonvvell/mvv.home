<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ModuleContactsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $moduleContacts \app\models\ModuleContacts[] */
/** @var $contact \app\models\ModuleContacts */

$this->title = Yii::t('app', 'Module Contacts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-contacts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Module Contacts'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <div class="file file__main-container">
        <?php
        foreach ($moduleContacts as $contact):?>
            <div class="file__main-container--tile">
                <div class="file__file-container">
                    <div class="file__file"
                         style="background: url('/<?php echo $contact->getPhotoAddress() ?>')"></div>
                </div>
                <div class="file__file-info">
                    <button class="common-button">Інформація</button>
                </div>
            </div>

        <?php endforeach; ?>
    </div>
    <?php echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>
    <?php Pjax::end(); ?></div>
