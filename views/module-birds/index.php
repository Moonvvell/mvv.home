<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $pagination yii\data\Pagination; */
/* @var $items \app\models\ModuleBirds[] */
/* @var $item \app\models\ModuleBirds */
$this->title = Yii::t('app', 'Module Birds');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-birds-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Module Birds'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['enablePushState' => false, 'enableReplaceState' => true]); ?>
    <div class="module-birds tile-container">
        <?php foreach ($items as $item): ?>
            <div class="module-birds__tile tile">
                <div class="module-birds__photo-container tile__image">
                    <div class="tile__file" style="background: url('/<?php echo $item->getMainPhoto(); ?>')"></div>
                </div>
                <div class="module-birds__name tile__title">
                    <span><?php echo $item->getFormattedTitle(); ?></span>
                </div>
                <div class="module-birds__actions tile__actions">
                    <div class="tile__actions--button hover-info delete"
                         data-hover-text="Видалити"
                         data-hover-color="<?php echo Yii::$app->user->identity->color ?>"
                         role="button" tabindex="1"
                         data-item-id="<?php echo $item->id; ?>">
                        <a href="/module-birds/delete?id=<?php echo $item->id; ?>" data-message="Дійсно видалити?" data-color="<?php echo Yii::$app->user->identity->color ?>">
                            <div class="icon svg multiply relative"></div>
                        </a>
                    </div>
                    <div class="tile__actions--button hover-info"
                         data-hover-text="Переглянути"
                         data-hover-color="<?php echo Yii::$app->user->identity->color ?>"
                         role="button" tabindex="2"
                         data-item-id="<?php echo $item->id; ?>">
                        <a href="/module-birds/view?id=<?php echo $item->id; ?>">
                            <div class="icon svg edit relative"></div>
                        </a>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>
    <?php Pjax::end(); ?></div>
