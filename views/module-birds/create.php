<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ModuleBirds */

$this->title = Yii::t('app', 'Create Module Birds');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Module Birds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-birds-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
