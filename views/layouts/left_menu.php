<?php
/**
 * Created by PhpStorm.
 * User: ihor.kyryliuk
 * Date: 14.06.2018
 * Time: 08:31
 */

use yii\helpers\Html;?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold"><?php echo Yii::$app->user->identity->username; ?></strong>
                                    <b class="caret"></b>
                                </span>
                            </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a href="/user/mypage" class="common-button common-button__link"><?php echo Yii::t('app', 'User panel'); ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?php echo Html::beginForm(['/index/logout'], 'post', ['id' => 'logoutForm']) ?><?php echo Html::endForm(); ?>
                            <a href="#" class="global-logout">
                                <i class="fa fa-sign-out"></i> <?php echo Yii::t('app', 'Logout'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php \app\components\menu\MenuComponent::GetMenu()::render(); ?>
        </ul>
    </div>
</nav>
