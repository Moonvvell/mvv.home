<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <h1 class="logo-name">MVV.HOME</h1>
    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "{input}\n<div class=\"login-form__error\">{error}</div>",
        ],
        'options' => [
            'class' => 'm-t',
        ]
    ]); ?>

    <?= $form->field($model, 'username')->textInput([
        'autofocus' => true,
        'class' => 'form-control',
        'placeholder' => 'Логін',
        'required' => ''
    ])->label('') ?>
    <?= $form->field($model, 'password')->passwordInput([
        'class' => 'form-control',
        'placeholder' => 'Пароль',
        'required' => ''
    ])->label('') ?>
    <?= $form->field($model, 'rememberMe')->hiddenInput(['checked' => true]) ?>

    <?= Html::submitButton('Зайти до системи', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>

    <?php ActiveForm::end(); ?>
</div>