<?php
$this->title = 'Start';
?>
<div class="main-menu">
    <div class="main-menu__tile-block">
        <div class="tile-grid-item">
            <a href="/module-birds">
                <div class="tile-grid-item__tile">
                    <div class="tile-grid-item__photo" style="background-image: url(media/menu/4.jpg);"></div>
                    <div class="tile-grid-item__info">
                        <div>Птахи</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="main-menu__tile-block">
        <div class="tile-grid-item">
            <a href="/module-contacts">
                <div class="tile-grid-item__tile">
                    <div class="tile-grid-item__photo" style="background-image: url(media/menu/1.jpg);"></div>
                    <div class="tile-grid-item__info">
                        <div>Контакти</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="tile-grid-item">
            <a href="/module-contacts-groups">
                <div class="tile-grid-item__tile">
                    <div class="tile-grid-item__photo"
                         style="background-image: url(media/menu/2.jpg);"></div>
                    <div class="tile-grid-item__info">
                        <div>Групи контактів</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="tile-grid-item">
            <a href="/module-contacts-contacts">
                <div class="tile-grid-item__tile">
                    <div class="tile-grid-item__photo"
                         style="background-image: url(media/menu/3.jpg);"></div>
                    <div class="tile-grid-item__info">
                        <div>Контакти контактів</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="main-menu__tile-block">
        <div class="tile-grid-item">
            <a href="/user">
                <div class="tile-grid-item__tile">
                    <div class="tile-grid-item__photo" style="background-image: url(media/menu/4.jpg);"></div>
                    <div class="tile-grid-item__info">
                        <div>Користувачі</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="tile-grid-item">
            <a href="/roles">
            <div class="tile-grid-item__tile">
                <div class="tile-grid-item__photo" style="background-image: url(media/menu/5.jpg);"></div>
                <div class="tile-grid-item__info">
                    <div>Ролі файлів</div>
                </div>
            </div>
            </a>
        </div>
        <div class="tile-grid-item">
            <div class="tile-grid-item__tile">
                <div class="tile-grid-item__photo"
                     style="background-image: url(media/menu/6.jpg);"></div>
                <div class="tile-grid-item__info">
                    <div>Логи</div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-menu__tile-block">
        <div class="tile-grid-item">
            <a href="/file">
                <div class="tile-grid-item__tile">
                    <div class="tile-grid-item__photo" style="background-image: url(media/menu/4.jpg);"></div>
                    <div class="tile-grid-item__info">
                        <div>Файли</div>
                    </div>
                </div>
            </a>
        </div>
        <!--        <div class="tile-grid-item">-->
        <!--            <div class="tile-grid-item__tile">-->
        <!--                <div class="tile-grid-item__photo" style="background-image: url(media/menu/5.jpg);"></div>-->
        <!--                <div class="tile-grid-item__info">-->
        <!--                    <div>Ролі файлів</div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--        <div class="tile-grid-item">-->
        <!--            <div class="tile-grid-item__tile">-->
        <!--                <div class="tile-grid-item__photo"-->
        <!--                     style="background-image: url(media/menu/6.jpg);"></div>-->
        <!--                <div class="tile-grid-item__info">-->
        <!--                    <div>Логи</div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
    </div>
</div>