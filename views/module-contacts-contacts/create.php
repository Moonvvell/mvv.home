<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ModuleContactsContacts */

$this->title = Yii::t('app', 'Create Module Contacts Contacts');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Module Contacts Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-contacts-contacts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
