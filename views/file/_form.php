<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-info-form">
<!--    --><?php //Pjax::begin(['enablePushState' => false]); ?>

    <div class="file-info-form__file-container">
        <div class="file-info-form__file" style="background: url('/<?php echo $model->getFullPath() ?>')"></div>
    </div>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'file-info-form__form']]); ?>
    <div class="common-input__container-main-info">
        <?php if ($model->getPrevFileId()): ?>
            <div class="container-block__prev-file">
                <?php echo Html::beginTag('a', ['href' => '/file/showdetails?id=' . $model->getPrevFileId() . '&type=full']); ?>
                <div class="container-block__prev icon svg prev"></div>
                <?php echo Html::endTag('a'); ?>
            </div>
        <?php endif; ?>
        <?php if ($model->getNextFileId()): ?>
            <div class="container-block__next-file">
                <a href="/file/showdetails?id=<?php echo $model->getNextFileId(); ?>&type=full">
                    <div class="container-block__next icon svg next"></div>
                </a>
            </div>
        <?php endif; ?>
        <div class="common-input__container w-50">
            <div class="common-input helper-padding__LR--20">
                <?= $form->field($model, 'file_name')->textInput(['maxlength' => true, 'readonly' => true])->label("Назва файлу на диску") ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <?= $form->field($model, 'original_name')->textInput(['readonly' => true])->label("Оригінальна назва файлу") ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <?= $form->field($model, 'file_extension')->textInput(['maxlength' => true, 'readonly' => true])->label("Розширення файлу") ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <?= $form->field($model, 'file_mime')->textInput(['maxlength' => true, 'readonly' => true])->label("MIME") ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <div class="form-group field-file-user_id has-success">
                    <label class="control-label" for="file-user_name">Додано користувачем</label>
                    <input type="text" id="file-user_name" class="form-control"
                           value="<?php echo $model->getUser()->one()->username ?>" readonly="" aria-invalid="false">
                </div>
                <?= $form->field($model, 'user_id')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            </div>
        </div>
        <div class="common-input__container w-50">

            <div class="common-input helper-padding__LR--20">
                <div class="form-group field-file-user_id has-success">
                    <label class="control-label" for="file-full_path">Шлях до файлу на диску</label>
                    <input type="text" id="file-full_path" class="form-control"
                           value="<?php echo $model->getFullPath() ?>" readonly="" aria-invalid="false">
                </div>
                <?= $form->field($model, 'file_path')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <div class="form-group field-file-user_id has-success">
                    <label class="control-label" for="file-role_name">Роль файлу</label>
<!--                    <input type="text" id="file-role_name" class="form-control"-->
<!--                           value="--><?php //echo $model->getRole()->one()->name ?><!--" readonly="" aria-invalid="false">-->
                </div>
                <?= $form->field($model, 'role_id')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <?= $form->field($model, 'element_id')->textInput(['readonly' => true])->label("Прив'язано до елементу") ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <?= $form->field($model, 'element_table')->textInput(['maxlength' => true, 'readonly' => true])->label("Прив'язано до таблиці") ?>
            </div>
            <div class="common-input helper-padding__LR--20">
                <?= $form->field($model, 'add_date')->textInput(['readonly' => true])->label("Дата додання") ?>
            </div>
        </div>
    </div>
    <div class="common-input__container-main-submit">
        <div class="common-input helper-padding__LR--20 helper-padding__B--10">
            <?= $form->field($model, 'file_comment')->textInput(['maxlength' => true])->label("Коментар до файлу") ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Зберегти коментар до фото'), ['class' => 'common-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
<!--    --><?php //Pjax::end(); ?>
</div>
