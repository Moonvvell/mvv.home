<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 02.04.2017
 * Time: 10:57
 */
use yii\widgets\Pjax;

?>
<div class="file-info__container-block">
    <div class="container-block__header">
        <div class="container-block__close icon svg multiply"></div>
    </div>
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'info-form']); ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?php if (isset($show_notify)): ?>
        <script>
            $.mvvNotify({'type': '<?php echo $show_notify['type'];?>', 'results': '<?php echo $show_notify['message'];?>'})
        </script>
    <?php endif; ?>
    <?php Pjax::end(); ?>
</div>