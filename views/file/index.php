<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $pagination yii\data\Pagination; */
/* @var $files \app\models\File[] */
/** @var $file \app\models\File */
$this->title = Yii::t('app', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create File'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['enablePushState' => false, 'enableReplaceState' => true]); ?>
    <div class="files-full-list" id="filesEditList">
        <?php foreach ($files as $file) : ?>
            <div class="files-edit-list__image">
                <img src="/<?= $file->getFullPath() ?>">
                <div class="actions">
                    <div class="title">
                        <?= $file->getFullOriginalNameWithExtension() ?>
                    </div>
                    <div class="buttons">
                        <a class="btn btn-sm btn-success fancy-view" href="/<?= $file->getFullPath() ?>"
                           onclick="filesService.showGallery(event, this)">
                            <?= Yii::t('app', 'View') ?>
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>
    <?php Pjax::end(); ?></div>
