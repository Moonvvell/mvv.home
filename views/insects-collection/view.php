<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\InsectsCollection */

$this->title = $model->identificator;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Insects Collections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insects-collection-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'catch_date',
                'catch_user_name',
                'catch_country',
                'catch_place',
                'catch_place_description:ntext',
                'catch_place_coordinates',
                'definition_date',
                'definition_user_name',
                'latin_name:ntext',
                'other_name:ntext',
                'sex',
                'identificator:ntext',
                'collection_box:ntext',
                'add_date',
                'last_change_date',
            ],
        ]);
    } catch (Exception $e) {
    } ?>
    <?php /** @var \app\models\File[] $files */
    if (isset($files) && !empty($files)): ?>
        <div class="files-edit-list" id="filesEditList">
            <?php foreach ($files as $file) : ?>
                <div class="files-edit-list__image">
                    <img src="/<?= $file->getFullPath() ?>">
                    <div class="actions">
                        <div class="title">
                            <?= $file->getFullOriginalNameWithExtension() ?>
                        </div>
                        <div class="buttons">
                            <a class="btn btn-sm btn-success fancy-view" href="/<?= $file->getFullPath() ?>"
                               onclick="filesService.showGallery(event, this)">
                                <?= Yii::t('app', 'View') ?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

</div>
