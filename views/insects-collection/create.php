<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InsectsCollection */

$this->title = Yii::t('app', 'Create Insects Collection');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Insects Collections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insects-collection-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
