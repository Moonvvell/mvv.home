<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsectsCollectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $pageData \app\models\InsectsCollection[] */

$this->title = Yii::t('app', 'Insects Collections');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row insects-collection-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create Insects Collection'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><?php echo Yii::t("app", "List of insects in collection") ?></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#">Config option 1</a>
                    </li>
                    <li><a href="#">Config option 2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <?php Pjax::begin(); ?>
            <?php try {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'identificator',
                        'catch_country',
                        'catch_place',
                        'latin_name',
                        'catch_date',

                        \app\components\TableActionsComponent::getActionButtonsForGridView(\app\components\TableActionsComponent::$allButtons, 'insects-collection')
                    ],
                ]);
            } catch (Exception $e) {
                var_dump($e);
            } ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
