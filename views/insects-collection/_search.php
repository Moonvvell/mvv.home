<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InsectsCollectionSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>
<input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">
<?php ActiveForm::end(); ?>

