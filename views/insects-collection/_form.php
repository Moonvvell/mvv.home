<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InsectsCollection */
/* @var $form yii\widgets\ActiveForm */
/* @var $files \app\models\File[] */

?>

<div class="row insects-collection-form">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => "{label}<div class='col-sm-10'>{input}</div>\n<div class=\"col-sm-push-2 col-sm-10\">{error}</div>",
                'labelOptions' => ['class' => 'col-sm-2 control-label']
            ],
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>
        <div class="tabs-container">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#tab-1"><?php echo Yii::t('app', 'Catch data'); ?></a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-2"><?php echo Yii::t('app', 'Basic info'); ?></a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-3"><?php echo Yii::t('app', 'Definition data'); ?></a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-4"><?php echo Yii::t('app', 'Images'); ?></a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <?= $form->field($model, 'catch_date')->textInput(['type' => 'datetime', 'class' => 'form-control datepicker-init']) ?>
                            <?= $form->field($model, 'catch_user_name')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'catch_country')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'catch_place')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'catch_place_description')->textarea(['rows' => 6]) ?>
                            <?= $form->field($model, 'catch_place_coordinates')->textInput(['maxlength' => true]) ?>
                        </fieldset>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <?= $form->field($model, 'identificator')->textInput(['class' => 'form-control']) ?>
                            <?= $form->field($model, 'collection_box')->textInput(['class' => 'form-control']) ?>
                        </fieldset>
                    </div>
                </div>
                <div id="tab-3" class="tab-pane">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <?= $form->field($model, 'definition_date')->textInput(['type' => 'datetime', 'class' => 'form-control datepicker-init']) ?>
                            <?= $form->field($model, 'definition_user_name')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'latin_name')->textInput(['class' => 'form-control']) ?>
                            <?= $form->field($model, 'other_name')->textInput(['class' => 'form-control']) ?>
                            <?= $form->field($model, 'sex')->dropDownList(
                                [
                                    0 => Yii::t('app', 'unknown'),
                                    1 => Yii::t('app', 'male'),
                                    2 => Yii::t('app', 'female')
                                ]) ?>
                        </fieldset>
                    </div>
                </div>
                <?php if (isset($fileForm)): ?>
                <div id="tab-4" class="tab-pane">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <?php if (isset($files) && !empty($files)): ?>
                                <div class="files-edit-list" id="filesEditList">
                                    <?php foreach ($files as $file) : ?>
                                        <div class="files-edit-list__image">
                                            <img src="/<?= $file->getFullPath() ?>">
                                            <div class="actions">
                                                <div class="title">
                                                    <?= $file->getFullOriginalNameWithExtension() ?>
                                                </div>
                                                <div class="buttons">
                                                    <a class="btn btn-sm btn-success fancy-view" href="/<?= $file->getFullPath() ?>"
                                                       onclick="filesService.showGallery(event, this)">
                                                        <?= Yii::t('app', 'View') ?>
                                                    </a>
                                                    <a class="btn btn-sm btn-danger" onclick="filesService.removeFile('remove-file?collectionId=<?= $model->id ?>&id=<?= $file->id
                                                    ?>', $(this))">
                                                        <?= Yii::t('app', 'Delete') ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>


                            <div class="user-page-container__ava-upload-buttons">
                                <?= $field = $form->field($fileForm, 'imageFile', ['enableClientValidation' => false])->fileInput(['class' => 'mvv-file-input'])->label(false);
                                $field->enableClientValidation = false;
                                $field->enableAjaxValidation = false;
                                ?>
                                <?= Html::submitButton(Yii::t('app', 'Завантажити'), ['class' => 'common-button common-button__border-right']) ?>
                            </div>

                            <!--                            <script>    $(".mvv-file-input").mvvFileInput();</script>-->
                        </fieldset>
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
</div>
