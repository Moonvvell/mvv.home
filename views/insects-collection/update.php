<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InsectsCollection */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Insects Collection',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Insects Collections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="insects-collection-update">
    <?= $this->render('_form', [
        'model' => $model,
        'fileForm' => $fileForm,
        'files' => $files
    ]) ?>
</div>
