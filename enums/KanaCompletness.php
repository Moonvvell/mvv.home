<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 25.03.2018
 * Time: 15:17
 */

namespace app\enums;

abstract class KanaCompletness extends BasicEnum
{
    const Full = 0;
    const Base = 1;
    const HanDakuten = 2;
    const Yoon = 3;
    const YoonHanDakuten = 4;

    const FullText = 'full';
    const BaseText = 'base';
    const HanDakutenText = 'hanDakuten';
    const YoonText = 'yoon';
    const YoonHanDakutenText = 'yoonHanDakuten';

    const HIRAGANA = 'hiragana';
    const KATAKANA = 'katakana';
}
