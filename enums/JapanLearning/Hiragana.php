<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 25.03.2018
 * Time: 15:25
 */

namespace app\enums\JapanLearning;


use app\enums\BasicEnum;

class Hiragana extends BasicEnum
{
    public static $base = [
        'a' => 'あ',
        'i' => 'い',
        'u' => 'う',
        'e' => 'え',
        'o' => 'お',
        'ka' => 'か',
        'ki' => 'き',
        'ku' => 'く',
        'ke' => 'け',
        'ko' => 'こ',
        'sa' => 'さ',
        'shi' => 'し',
        'su' => 'す',
        'se' => 'せ',
        'so' => 'そ',
        'ta' => 'た',
        'chi' => 'ち',
        'tsu' => 'つ',
        'te' => 'て',
        'to' => 'と',
        'na' => 'な',
        'ni' => 'に',
        'nu' => 'ぬ',
        'ne' => 'ね',
        'no' => 'の',
        'ha' => 'は',
        'hi' => 'ひ',
        'fu' => 'ふ',
        'he' => 'へ',
        'ho' => 'ほ',
        'ma' => 'ま',
        'mi' => 'み',
        'mu' => 'む',
        'me' => 'め',
        'mo' => 'も',
        'ya' => 'や',
        'yu' => 'ゆ',
        'yo' => 'よ',
        'ra' => 'ら',
        'ri' => 'り',
        'ru' => 'る',
        're' => 'れ',
        'ro' => 'ろ',
        'wa' => 'わ',
        'wo' => 'を',
        'n' => 'ん'
    ];
    public static $hanDakuten = [
        'ga' => 'が',
        'gi' => 'ぎ',
        'gu' => 'ぐ',
        'ge' => 'げ',
        'go' => 'ご',
        'za' => 'ざ',
        'ji (s)' => 'じ',
        'dzu (s)' => 'ず',
        'ze' => 'ぜ',
        'zo' => 'ぞ',
        'da' => 'だ',
        'ji (t)' => 'ぢ',
        'dzu (t)' => 'づ',
        'de' => 'で',
        'do' => 'ど',
        'ba' => 'ば',
        'bi' => 'び',
        'bu' => 'ぶ',
        'be' => 'べ',
        'bo' => 'ぼ',
        'pa' => 'ぱ',
        'pi' => 'ぴ',
        'pu' => 'ぷ',
        'pe' => 'ぺ',
        'po' => 'ぽ'
    ];
    public static $yoon = [
        'kya' => 'きゃ',
        'kyu' => 'きゅ',
        'kyo' => 'きょ',
        'shya' => 'しゃ',
        'shyu' => 'しゅ',
        'shyo' => 'しょ',
        'chya' => 'ちゃ',
        'chyu' => 'ちゅ',
        'chyo' => 'ちょ',
        'nya' => 'にゃ',
        'nyu' => 'にゅ',
        'nyo' => 'にょ',
        'hya' => 'ひゃ',
        'hyu' => 'ひゅ',
        'hyo' => 'ひょ',
        'mya' => 'みゃ',
        'myu' => 'みゅ',
        'myo' => 'みょ',
        'rya' => 'りゃ',
        'ryu' => 'りゅ',
        'ryo' => 'りょ'
    ];
    public static $yoonHanDakuten = [
        'gya' => 'ぎゃ',
        'gyu' => 'ぎゅ',
        'gyo' => 'ぎょ',
        'jya (s)' => 'じゃ',
        'jyu (s)' => 'じゅ',
        'jyo (s)' => 'じお',
        'jya (t)' => 'ぢゃ',
        'jyu (t)' => 'ぢゅ',
        'jyo (t)' => 'ぢょ',
        'bya' => 'びゃ',
        'byu' => 'びゅ',
        'byo' => 'びょ',
        'pya' => 'ぴゃ',
        'pyu' => 'ぴゅ',
        'pyo' => 'ぴょ'
    ];
}
