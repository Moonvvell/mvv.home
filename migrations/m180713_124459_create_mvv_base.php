<?php

use yii\db\Migration;

/**
 * Class m180713_124459_create_mvv_base
 */
class m180713_124459_create_mvv_base extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'file', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(45)->notNull(),
            'file_path' => $this->string(45)->defaultValue(NULL),
            'file_extension' => $this->string(45)->notNull(),
            'role_id' => $this->integer()->defaultValue(NULL),
            'element_id' => $this->integer()->defaultValue(NULL),
            'element_table' => $this->string(45)->defaultValue(NULL),
            'user_id' => $this->integer()->defaultValue(NULL),
            'original_name' => $this->text()->defaultValue(NULL),
            'add_date' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'file_mime' => $this->string(45)->defaultValue(NULL),
            'file_comment' => $this->text()->defaultValue(NULL)
            ]);

        $this->createTable(
            'module_contacts_groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'color' => $this->string(45)->defaultValue('#ffffff')
        ]);

        $this->createTable(
            'module_contacts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'father_name' => $this->string(45)->defaultValue(NULL),
            'last_name' => $this->string(45)->defaultValue(NULL),
            'birthday' => $this->date()->defaultValue(NULL),
            'gender' => $this->integer(2)->defaultValue(NULL),
            'photo_id' => $this->integer(10)->defaultValue(NULL),
            'contact_group_id' => $this->integer(10)->defaultValue(NULL),
            'comment' => $this->string(500)->defaultValue(NULL)
        ]);

        $this->createTable(
            'module_contacts_contacts', [
            'id' => $this->primaryKey(),
            'contact_id' => $this->integer()->defaultValue(NULL),
            'value' => $this->string(45)->defaultValue(NULL),
            'type' => $this->integer(10)->defaultValue('1')
        ]);
        $this->createTable(
            'user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(45)->notNull(),
            'password' => $this->string(255)->notNull(),
            'color' => $this->string(45)->defaultValue('#ffffff'),
            'rights' => $this->integer(10)->notNull()->defaultValue('0'),
            'session_lifetime' => $this->integer(11)->notNull()->defaultValue('3600'),
            'user_avatar_id' => $this->integer()->defaultValue(NULL),
            'creation_date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
        $this->insert('user', [
            'username' => 'admin',
            'password' => $hash = Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'rights' => '1',
            'session_lifetime' => '3600'
        ]);
        $this->createTable(
            'module_has_file', [
            'id' => $this->primaryKey(),
            'file_id' => $this->integer()->defaultValue(NULL),
            'element_id' => $this->integer()->defaultValue(NULL),
            'element_table' => $this->string(45)->defaultValue(NULL),
            'role_id' => $this->integer()->defaultValue(NULL)
        ]);
        $this->createTable(
            'role', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'slug' => $this->string(45)->notNull(),
            'type' => $this->string(45)->notNull()
        ]);
        $this->createTable(
            'insects_collection_has_files', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer()->notNull(),
            'file_id' => $this->integer()->notNull()
        ]);
        $this->createTable(
            'insects_collection', [
            'id' => $this->primaryKey(),
            'catch_date' => $this->timestamp()->notNull()->defaultValue('0000-00-00 00:00:00'),
            'catch_user_name' => $this->string(225)->defaultValue(NULL),
            'catch_country' => $this->string(225)->defaultValue(NULL),
            'catch_place' => $this->string(225)->defaultValue(NULL),
            'catch_place_description' => $this->text()->defaultValue(NULL),
            'catch_place_coordinates' => $this->string(100)->defaultValue(NULL),
            'definition_date' => $this->timestamp()->notNull()->defaultValue('0000-00-00 00:00:00'),
            'definition_user_name' => $this->string(255)->defaultValue(NULL),
            'latin_name' => $this->text()->defaultValue(NULL),
            'other_name' => $this->text()->defaultValue(NULL),
            'sex' => $this->boolean()->defaultValue(NULL),
            'identificator' => $this->text()->defaultValue(NULL),
            'collection_box' => $this->text()->defaultValue(NULL),
            'add_date' => $this->timestamp()->notNull()->defaultValue('0000-00-00 00:00:00'),
            'last_change_date' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')
        ]);

        $this->createTable(
            'module_birds_has_note', [
            'id' => $this->primaryKey(),
            'id_module' => $this->integer()->notNull(),
            'id_note' => $this->integer()->notNull()
        ]);

        $this->createTable(
            'notes', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->defaultValue(NULL),
            'add_date' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'order' => $this->integer(11)->defaultValue(NULL)
        ]);

        $this->createTable(
            'module_birds', [
            'id' => $this->primaryKey(),
            'latin_name' => $this->string(350)->notNull()->defaultValue(NULL),
            'common_name' => $this->string(350)->defaultValue(NULL),
            'seen_place' => $this->text()->defaultValue(NULL),
            'seen_date' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
        $this->addForeignKey('fk_file_to_user',
            'file',
            'user_id',
            'user',
            'id',
            'SET NULL',
            'CASCADE');
        $this->addForeignKey('fk_ichf_to_file', 'insects_collection_has_files', 'file_id', 'file', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_ichf_to_ic', 'insects_collection_has_files', 'item_id', 'insects_collection', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_module_has_to_role', 'module_has_file', 'role_id', 'role', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_module_to_file', 'module_has_file', 'file_id', 'file', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_contacts_groups', 'module_contacts', 'contact_group_id', 'module_contacts_groups', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_contacts_photo', 'module_contacts', 'photo_id', 'file', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_contact','module_contacts_contacts','contact_id','module_contacts','id','RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_module_birds_to_note', 'module_birds_has_note', 'id_module', 'module_birds', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_module_note_to_note', 'module_birds_has_note', 'id_note', 'notes', 'id', 'RESTRICT', 'RESTRICT');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180713_124459_create_mvv_base cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180713_124459_create_mvv_base cannot be reverted.\n";

        return false;
    }
    */
}
