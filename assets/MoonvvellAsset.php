<?php

namespace app\assets;

use yii\web\AssetBundle;

class MoonvvellAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/bootstrap.min.css',
        'font-awesome/css/font-awesome.css',
        'css/animate.css',
        'css/plugins/dataTables/datatables.min.css',
        'css/plugins/datapicker/datepicker3.css',
        'css/plugins/blueimp/css/blueimp-gallery.min.css',
        'css/style.css',
    ];
    public $js = [
//        'js/jquery-3.1.1.min.js',
        'node_modules/vue/dist/vue.js',
        'js/bootstrap.min.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/plugins/dataTables/datatables.min.js',
        'js/plugins/datapicker/bootstrap-datepicker.js',
        'js/plugins/daterangepicker/daterangepicker.js',
        'js/plugins/blueimp/jquery.blueimp-gallery.min.js',

        'js/inspinia.js',
        'js/plugins/pace/pace.min.js',

        'js/modules/tableService.js',
        'js/modules/kanaLearnService.js',
        'js/modules/filesService.js',
        'js/modules/insectsLabelsService.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
