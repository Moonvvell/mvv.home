<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 12.02.2018
 * Time: 19:01
 */

namespace app\controllers;


use app\enumDataHelpers\JapanLearning\KanaLearning;
use app\enumDataHelpers\KanaLearnEnumHelper;
use yii\web\Controller;
class KanaLearnController extends Controller
{

    /**
     * Lists all InsectsCollection models.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $fullData = KanaLearnEnumHelper::getAllSymbolsAsGroupedArray();
        return $this->render('index', [
            'kanaData' => KanaLearning::getInstance()->getFullList(),
            'arrayData' => ['array' => KanaLearning::getInstance()->getArrayList()]
        ]);
    }
}
