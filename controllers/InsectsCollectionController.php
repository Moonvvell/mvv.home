<?php

namespace app\controllers;

use app\models\InsectsCollectionHasFiles;
use Yii;
use app\models\InsectsCollection;
use app\models\InsectsCollectionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\web\UploadedFile;

/**
 * InsectsCollectionController implements the CRUD actions for InsectsCollection model.
 */
class InsectsCollectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InsectsCollection models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = InsectsCollection::find();
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count]);
        $pagination->setPageSize(15);
        $pageData = $query->offset($pagination->offset)
            ->limit($pagination->limit)->orderBy(['id' => SORT_DESC])
            ->all();


        $searchModel = new InsectsCollectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'pageData' => $pageData,
            'searchModel' => $searchModel,
            'pagination' => $pagination,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single InsectsCollection model.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $files = $model->getFiles();
        return $this->render('view', [
            'model' => $model,
            'files' => $files
        ]);
    }

    /**
     * Creates a new InsectsCollection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \RuntimeException
     */
    public function actionCreate()
    {
        $model = new InsectsCollection();

        if ($model->load(Yii::$app->request->post())) {
            $model->add_date = date('Y-m-d H:i:s');
            $model->generateId();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Clone existing InsectsCollection model.
     * If creation is successful, the browser will be redirected to the 'list' page.
     * @return mixed
     * @throws \RuntimeException
     * @throws NotFoundHttpException
     */
    public function actionClone($id)
    {
        $model = $this->findModel($id);

        if ($model) {
            $new = $model->duplicate();
        }
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing InsectsCollection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $fileForm = new \app\models\UploadForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->last_change_date = date('Y-m-d H:i:s');
            if ($model->identificator === '-') {
                $model->generateId();
            }
            $model->save();
            $fileForm->imageFile = UploadedFile::getInstance($fileForm, 'imageFile');
            $fileForm->upload(2, $model->id, InsectsCollection::tableName(), InsectsCollectionHasFiles::className());
            $files = $model->getFiles();
            return $this->redirect(
                [
                    'view',
                    'id' => $model->id,
                    'files' => $files
                ]);
        }
        $files = $model->getFiles();
        return $this->render('update', [
            'model' => $model,
            'fileForm' => $fileForm,
            'files' => $files
        ]);
    }

    /**
     * Deletes an existing InsectsCollection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InsectsCollection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InsectsCollection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InsectsCollection::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRemoveFile($collectionId, $id)
    {
        try {
            InsectsCollectionHasFiles::deleteAll(['file_id' => $id, 'item_id' => $collectionId]);
            return json_encode(['success' => true]);
        } catch (\Exception $exception) {
            return json_encode(['success' => false, 'exception' => $exception->getMessage()]);
        }

    }
}
