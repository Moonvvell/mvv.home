<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 23.09.2018
 * Time: 17:12
 */

namespace app\controllers;


use app\models\insectslabels\InsectGeoLabel;
use app\models\insectslabels\InsectIdentifyLabel;
use app\models\insectslabels\InsectLabel;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use yii\web\Controller;

class InsectsLabelsController extends Controller
{
    private $geoPath = './labels_json/geo';
    private $identifyPath = './labels_json/identify';

    public function actionIndex()
    {
        $geoFileNames = [];
        /** @var \SplFileInfo $filename */
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->geoPath)) as $filename) {
            // filter out "." and ".."
            if ($filename->isDir()) {
                continue;
            }
            $geoFileNames[] = $filename->getFilename();
        }
        $identifyNames = [];
        /** @var \SplFileInfo $filename */
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->identifyPath)) as $filename) {
            // filter out "." and ".."
            if ($filename->isDir()) {
                continue;
            }
            $identifyNames[] = $filename->getFilename();
        }
        return $this->render('index', [
            'geoFileNames' => $geoFileNames,
            'identifyNames' => $identifyNames
        ]);
    }

    public function actionGeoLabels($filename)
    {
        $this->layout = 'labels';
        $fileContent = json_decode(file_get_contents($this->geoPath . '/' . $filename));
        $models = array_map(function ($item) {
            return new InsectLabel($item);
        }, $fileContent->items);
        return $this->render('labels', ['data' => $models]);
    }

    public function actionIdentifyLabels($filename)
    {
        $this->layout = 'labels';
        $fileContent = json_decode(file_get_contents($this->identifyPath . '/' . $filename));
        $models = array_map(function ($item) {
            return new InsectIdentifyLabel($item);
        }, $fileContent->items);
        return $this->render('identifyLabels', ['data' => $models]);
    }
}
