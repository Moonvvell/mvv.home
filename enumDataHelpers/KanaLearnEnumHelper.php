<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 12.02.2018
 * Time: 19:09
 */

namespace app\enumDataHelpers;

use app\enums\JapanLearning\Hiragana;
use app\enums\JapanLearning\Katakana;
use app\enums\KanaCompletness;

class KanaLearnEnumHelper
{
    /**
     * @param int $completeness
     * @return array
     */
    public static function getHiraganaSymbols($completeness = KanaCompletness::Full)
    {
        switch ($completeness) {
            case KanaCompletness::Full:
                return [
                    KanaCompletness::BaseText => Hiragana::$base,
                    KanaCompletness::HanDakutenText => Hiragana::$hanDakuten,
                    KanaCompletness::YoonText => Hiragana::$yoon,
                    KanaCompletness::YoonHanDakutenText => Hiragana::$yoonHanDakuten,
                ];
            case KanaCompletness::Base:
                return Hiragana::$base;
            case KanaCompletness::HanDakuten:
                return Hiragana::$hanDakuten;
            case KanaCompletness::Yoon:
                return Hiragana::$yoon;
            case KanaCompletness::YoonHanDakuten:
                return Hiragana::$yoonHanDakuten;
            default:
                return [];
        }
    }

    /**
     * @param int $completeness
     * @return array
     */
    public static function getKatakanaSymbols($completeness = KanaCompletness::Full)
    {
        switch ($completeness) {
            case KanaCompletness::Full:
                return [
                    KanaCompletness::BaseText => Katakana::$base,
                    KanaCompletness::HanDakutenText => Katakana::$hanDakuten,
                    KanaCompletness::YoonText => Katakana::$yoon,
                    KanaCompletness::YoonHanDakutenText => Katakana::$yoonHanDakuten,
                ];
            case KanaCompletness::Base:
                return Katakana::$base;
            case KanaCompletness::HanDakuten:
                return Katakana::$hanDakuten;
            case KanaCompletness::Yoon:
                return Katakana::$yoon;
            case KanaCompletness::YoonHanDakuten:
                return Katakana::$yoonHanDakuten;
            default:
                return [];
        }
    }

    public static function getAllSymbolsAsGroupedArray()
    {
        return [
          KanaCompletness::HIRAGANA => self::getHiraganaSymbols(),
          KanaCompletness::KATAKANA => self::getKatakanaSymbols()
        ];
    }
}
