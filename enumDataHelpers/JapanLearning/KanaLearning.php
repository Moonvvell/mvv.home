<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 25.03.2018
 * Time: 16:06
 */

namespace app\enumDataHelpers\JapanLearning;


use app\enumDataHelpers\KanaLearnEnumHelper;

class KanaLearning
{
    /** @var KanaLearning */
    private static $instance = null;

    /** @var KanaItem[] */
    private $kanasList = [];

    private $arrayKanaList = [];

    /**
     * KanaLearning constructor.
     */
    public function __construct()
    {
        $array = KanaLearnEnumHelper::getAllSymbolsAsGroupedArray();
        foreach ($array as $type => $typeList) {
            foreach ($typeList as $subType => $items) {
                foreach ($items as $name => $symbol) {
                    $kanaData = [
                        'name' => $name,
                        'symbol' => $symbol,
                        'subType' => $subType,
                        'type' => $type
                    ];
                    $kanaItem = new KanaItem($kanaData);
                    $this->kanasList[] = $kanaItem;
                    $this->arrayKanaList[] = $kanaData;
                }
            }
        }
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getFullList()
    {
        return $this->kanasList;
    }

    public function getArrayList()
    {
        return $this->arrayKanaList;
    }
}

class KanaItem
{
    /** @var string */
    private $name;
    /** @var string */
    private $symbol;
    /** @var integer */
    private $kanaSubType;
    /** @var integer */
    private $kanaType;

    /**
     * KanaItem constructor.
     * @param $kanaData
     */
    public function __construct($kanaData)
    {
        $this->name = $kanaData['name'];
        $this->symbol = $kanaData['symbol'];
        $this->kanaSubType = $kanaData['subType'];
        $this->kanaType = $kanaData['type'];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function getKanaType()
    {
        return $this->kanaType;
    }

    public function getKanaSubType()
    {
        return $this->kanaSubType;
    }
}
