/**
 * Created by moonvvell on 16.04.16.
 */
;(function ($) {
    $.fn.mvvSelect = function () {
        return this.each(function () {
            let $this = $(this);
            let options = $this.find('option');
            let mvvSelectObject = $('<div class="mvvSelectObject"></div>');
            let ulSelect = $('<ul class="mvvUlSelect"></ul>');
            let ulDiv = $('<div class="mvvUlDiv"></div>');

            function init() {
                let firstOption = true;

                options.each(function () {
                    if ($(this).val() && $(this).text()) {
                        firstOption = false;
                        let liElement = $('<li class="mvvLiSelectElement"></li>');
                        liElement
                            .data('value', $(this).val())
                            .text($(this).text());
                        ulSelect.append(liElement);
                        bindLi(liElement);

                        if (firstOption || $(this).is(':selected')) {
                            ulDiv.text($(this).text());
                        }
                    }
                });

                ulSelect.hide();
                mvvSelectObject.append(ulDiv).append(ulSelect);
                $this.after(mvvSelectObject);

                let $newWidth = $this.width() > 0 ? $this.width() : '100%';
                mvvSelectObject.width($newWidth);

                createNiceUl();
                $this.addClass('mvvSelectActive').hide();
            }

            function bindLi(element) {
                $(element).on('click', function () {
                    $this.val($(this).data('value'));
                    ulDiv.text($(this).text());
                    $this.change();
                });
            }

            function createNiceUl() {
                $('body').on('click', function () {
                    hideAllUl();
                });
                mvvSelectObject.on('click', function (event) {
                    event.stopPropagation();
                    if (ulSelect.is(':visible')) {
                        hideUl();
                    } else {
                        hideAllUl();
                        showUl();
                    }
                })
            }

            function hideUl() {
                ulSelect.hide().height(0).css('z-index', 'null');
            }

            function showUl() {
                ulSelect.show().height(getAllLiHeight()).css('z-index', '1000');
                markChecked();
            }

            function hideAllUl() {
                $('.mvvUlSelect').hide();
            }

            function markChecked() {
                $('.mvvLiSelectElement').removeClass('checked');
                ulSelect.find('li').each(function () {
                    if ($this.val() === $(this).data('value')) {
                        $(this).addClass('checked');
                    }
                });
            }

            function getAllLiHeight() {
                let $allHeight = 0;
                ulSelect.find('li').each(function () {
                    $allHeight += $(this).height();
                });
                return $allHeight;
            }

            init();
        });
    };
})(jQuery);
(function ($) {
    $.fn.mvvFileInput = function () {
        return this.each(function () {
            let $this = $(this);
            console.log($this);
            let mvvFileInputContainerObject = $('<div class="mvvFileInputContainer"></div>');
            let mvvFileInputButtonObject = $('<button class="mvvFileInputButton">Вибрати файл</button>');
            let mvvFileInputLabelObject = $('<div class="mvvFileInputLabel">Не вибрано жодного файлу</div>');
            let ulSelect = $('<ul class="mvvUlSelect"></ul>');
            let ulDiv = $('<div class="mvvUlDiv"></div>');

            function init() {

                mvvFileInputContainerObject.append(mvvFileInputButtonObject);
                mvvFileInputContainerObject.append(mvvFileInputLabelObject);
                $this.after(mvvFileInputContainerObject);

                bindAction();
                // let $newWidth = $this.width() > 0 ? $this.width() : '100%';
                // mvvFileInputContainerObject.width($newWidth);
                $this.addClass('mvvFileInputActive').hide();
            }

            function bindAction() {
                $(mvvFileInputButtonObject).on('click', function (e) {
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    $this.click();
                });
                $this.on('change', function (e) {
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    let $label = $this.val();
                    mvvFileInputLabelObject.text($label);
                })
            }

            if (!$this.hasClass('mvvFileInputActive')) {
                init();
            }
        });
    };
})(jQuery);

(function ($) {
    $.mvvNotify = function (json) {
        let type = json.type;
        let message = json.results;
        let container = $('<div class="mvvNotify ' + json.type.toLowerCase() + '"></div>');
        let span = $('<span></span>');
        span.text(type + ": " + message);
        container.html(span);
        $('body').append(container);
        container.fadeIn("fast");
        setTimeout(function () {
            container.fadeOut("fast");
            $(container).remove();
        }, 4000)

    };
})(jQuery);

(function ($) {
    $.fn.mvvHoverInfo = function () {
        return this.each(function () {
            let $this = $(this);
            let $infoContainer = $('<div class="mvvHoverInfoContainer"></div>');
            let $infoTextContainer = $('<div class="mvvHoverInfoTextContainer"></div>');

            function init() {
                let $color = $this.data('hover-color');
                if ($color.length)
                    $infoContainer.css("background-color", $color);
                $infoTextContainer.text($this.data('hover-text'));
                $infoContainer.append($infoTextContainer);
                $this.append($infoContainer);
                $this.addClass('mvvHoverActiveInit');
                addListener();
            }

            function addListener() {
                $this.on('mouseover', function (evt) {
                    $infoContainer.css({
                        top: evt.pageY + 15,
                        left: evt.pageX - 30
                    }).show();
                });

                $this.on('mouseleave', function (evt) {
                    $infoContainer.hide();
                });
            }

            if (!$this.hasClass('mvvHoverActiveInit')) {
                init();
            }
        })
    };
})(jQuery);


function mvvClock() {
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    let hours = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;

    let time = day + "." + month + "." + year + " " + hours + ":" + minutes + ":" + seconds
    $('.mvv-clock').html(time);
    setTimeout("mvvClock()", 1000);
}

function replaceSVG() {
    $('img.svg').each(function () {
        let $img = $(this);
        let imgID = $img.attr('id');
        let imgClass = $img.attr('class');
        let imgURL = $img.attr('src');

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            let $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}


$(document).ready(function () {

    $(".mvv-select").mvvSelect();
    $(".mvv-file-input").mvvFileInput();
    $(".hover-info").mvvHoverInfo();
    mvvClock();
    setTimeout("replaceSVG()", 1000);
});