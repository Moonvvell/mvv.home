/**
 * Created by Moonvvell on 31.03.2017.
 */
'use strict';
const mainService = {
    bindListeners() {
        $(document).on('click', '.global-logout', () => {
            console.log('click');
            $('#logoutForm').submit();
        })
    }
};

mainService.bindListeners();
tableService.init();

$(document).on('ready', () => {
    $('.dataTables-example').DataTable({
        language: {
            url: "/js/plugins/dataTables/i18n/ukrainian.lang"
        },
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Зкопіювати'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {
                extend: 'print',
                text: 'Надрукувати',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    });

    $('.datepicker-init').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd',
        toggleActive: false
    });
});
