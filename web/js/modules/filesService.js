'use strict';
let filesService = {
    init() {
    },
    showGallery(event, item) {
        event.preventDefault();
        let options = {index: item, event: event, carousel: false};
        let links = document.getElementsByClassName('fancy-view');
        blueimp.Gallery(links, options);
    },
    removeFile(link, item) {
        if (window.confirm('Вилучити?')) {
            let itemBlock = item.closest('.files-edit-list__image');
            $.get(link, (success) => {
                $(itemBlock).remove();
            })
        }
    }
};
