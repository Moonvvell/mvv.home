/**
 * Created by Moonvvell on 02.04.2017.
 */
'use strict';
const fileInfo = {
        init: function () {
            fileInfo.registerListener();
        },
        createWindow: function (fileid, infotype) {
            let $window = $('<div class="file-info__container"></div>');
            $window.load('/file/showdetails?id=' + fileid + '&type=' + infotype);
            return $window;
        },
        destroyWindow: function (window) {
            let $body = $('body');
            window.remove();
            $body.removeClass('helper-overflow__hidden');
        },
        registerListener: function () {
            $(document).on('click', '.open-file-info', function () {
                let $this = $(this);
                let fileId = $this.data('file-id');
                let infoType = $this.data('open-info');
                let $body = $('body');
                let $window = fileInfo.createWindow(fileId, infoType);
                $body.addClass('helper-overflow__hidden');
                $body.prepend($window);
                $window.on('click', '.container-block__close', function () {
                    fileInfo.destroyWindow($window);
                });
            });

        }
    }
;