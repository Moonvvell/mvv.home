'use strict';
let tableService = {
    init() {
    },
    showGallery(event, item) {
        event.preventDefault();
        let options = {index: item, event: event, carousel: false};
        let links = document.getElementsByClassName('fancy-view');
        blueimp.Gallery(links, options);
    }
};
