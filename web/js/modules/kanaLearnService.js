'use strict';
window.kanaLearnService = {
    init(initData) {
        new Vue({
            el: '#kanaLearnSection',
            data() {
                return {
                    initData,
                    showHiragana: true,
                    showHiraganaBase: true,
                    showHiraganaYoon: true,
                    showHiraganaHanDakuten: true,
                    showHiraganaYoonHanDakuten: true,
                    showKatakana: true,
                    showKatakanaBase: true,
                    showKatakanaYoon: true,
                    showKatakanaHanDakuten: true,
                    showKatakanaYoonHanDakuten: true,
                    showFullList: false,
                    learning: false,
                    kanaNumber: 80,
                    generatedData: null
                }
            },
            methods: {
                getKanaData() {
                    let items = [];
                    let data = this.initData.array;
                    data.forEach((item, idx) => {
                        if (this.needShowIt(item)) {
                            items.push(item);
                        }
                    });
                    if (!this.showFullList) {
                        if (this.generatedData === null) {
                            this.filterData(items);
                        }
                        return this.generatedData
                    } else {
                        this.generatedData = null
                    }
                    return items;
                },
                filterData(data) {
                    this.generatedData = Array(this.kanaNumber).fill().map(() => data[Math.round(Math.random() * data.length)]);
                },
                needShowIt(kanaItem) {
                    if (!kanaItem) {
                        return false
                    }
                    let kanaType = kanaItem.type;
                    let kanaSubType = kanaItem.subType;
                    let canShowThisHiraganaType = kanaType === 'hiragana' && this.canShowHiragana;
                    let canShowThisHiraganaSubType = (kanaSubType === 'base' && this.showHiraganaBase) ||
                        (kanaSubType === 'yoon' && this.showHiraganaYoon) ||
                        (kanaSubType === 'hanDakuten' && this.showHiraganaHanDakuten) ||
                        (kanaSubType === 'yoonHanDakuten' && this.showHiraganaYoonHanDakuten);
                    let canShowThisKatakanaType = kanaType === 'katakana' && this.canShowKatakana;
                    let canShowThisKatakanaSubType = (kanaSubType === 'base' && this.showKatakanaBase) ||
                        (kanaSubType === 'yoon' && this.showKatakanaYoon) ||
                        (kanaSubType === 'hanDakuten' && this.showKatakanaHanDakuten) ||
                        (kanaSubType === 'yoonHanDakuten' && this.showKatakanaYoonHanDakuten);
                    return (canShowThisHiraganaType && canShowThisHiraganaSubType) || (canShowThisKatakanaType && canShowThisKatakanaSubType);
                },
                toggleLearning() {
                    this.learning = !this.learning;
                },
                generateItems() {
                    this.generatedData = null
                },
                showSymbolIfHidden($event) {
                    if (this.learning) {
                        let item = $($event.target);
                        item.find('.hide-symbol').addClass('force-show');
                    }
                }
            },
            computed: {
                canShowHiragana: {
                    get() {
                        return this.showHiragana;
                    },
                    set(value) {
                        this.showHiragana = value
                    }
                },
                canShowHiraganaBase: {
                    get() {
                        return this.showHiraganaBase;
                    },
                    set(value) {
                        this.showHiraganaBase = value
                    }
                },
                canShowHiraganaYoon: {
                    get() {
                        return this.showHiraganaYoon;
                    },
                    set(value) {
                        this.showHiraganaYoon = value
                    }
                },
                canShowHiraganaHanDakuten: {
                    get() {
                        return this.showHiraganaHanDakuten;
                    },
                    set(value) {
                        this.showHiraganaHanDakuten = value
                    }
                },
                canShowHiraganaYoonHanDakuten: {
                    get() {
                        return this.showHiraganaYoonHanDakuten;
                    },
                    set(value) {
                        this.showHiraganaYoonHanDakuten = value
                    }
                },
                canShowKatakana: {
                    get() {
                        return this.showKatakana;
                    },
                    set(value) {
                        this.showKatakana = value
                    }
                },
                canShowKatakanaBase: {
                    get() {
                        return this.showKatakanaBase;
                    },
                    set(value) {
                        this.showKatakanaBase = value
                    }
                },
                canShowKatakanaYoon: {
                    get() {
                        return this.showKatakanaYoon;
                    },
                    set(value) {
                        this.showKatakanaYoon = value
                    }
                },
                canShowKatakanaHanDakuten: {
                    get() {
                        return this.showKatakanaHanDakuten;
                    },
                    set(value) {
                        this.showKatakanaHanDakuten = value
                    }
                },
                canShowKatakanaYoonHanDakuten: {
                    get() {
                        return this.showKatakanaYoonHanDakuten;
                    },
                    set(value) {
                        this.showKatakanaYoonHanDakuten = value
                    }
                },
                canShowFullList: {
                    get() {
                        return this.showFullList;
                    },
                    set(value) {
                        this.showFullList = value
                    }
                },
                startLearn: {
                    get() {
                        return this.learn;
                    },
                    set(value) {
                        this.learn = value
                    }
                },
                startLearnItems: {
                    get() {
                        return this.kanaNumber;
                    },
                    set(value) {
                        this.kanaNumber = parseInt(value)
                    }
                }
            }
        })
    }
};


$(document).ready(() => {
    if ($('#kanaLearnSection').length && window.kanaInitData) {
        window.kanaLearnService.init(window.kanaInitData)
    }
});
