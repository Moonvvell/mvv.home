/**
 * Created by Moonvvell on 06.04.2017.
 */
'use strict';
const controls = {
    init: function () {
        controls.registerListener();
    },
    registerListener: function () {
        $('.tile__actions').on('click', '.tile__actions--button.delete a', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            event.stopPropagation();
            controls.showConfirmation($(this).attr('href'), $(this).data('message'), $(this).data('color'));
        })
    },
    showConfirmation: function (link, message, color) {
        let window = $('<div id="confirmation-window" style="background-color:' + color + '"></div>');
        let windowBody = $('<div class="confirmation-window__body"><span>' + message + '</span></div>');
        let windowButtons = $('<div class="confirmation-window__buttons">' +
            '<div class="common-button common-button__border-left" onclick="$(\'#confirmation-window\').remove();">Відмінити</div>' +
            '<div class="common-button common-button__border-right" onclick="window.location = \'' + link + '\'">Так</div>' +
            '</div>');

        window.append(windowBody);
        window.append(windowButtons);
        $('body').append(window);
    }
};
