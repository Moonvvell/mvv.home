ALTER TABLE `file`
  CHANGE COLUMN `user_name` `user_id` INT(10) UNSIGNED NULL,
  ADD INDEX `fk_user_upload_file_idx` (`user_id` ASC);
ALTER TABLE `file`
  ADD CONSTRAINT `fk_user_upload_file`
FOREIGN KEY (`user_id`)
REFERENCES `user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `file`
  ADD COLUMN `original_name` VARCHAR(45) NULL
  AFTER `file_name`;

ALTER TABLE `moonvvell.home`.`file`
  ADD COLUMN `add_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP
  AFTER `element_table`;

ALTER TABLE `moonvvell.home`.`file`
  CHANGE COLUMN `original_name` `original_name` TEXT NULL DEFAULT NULL;
