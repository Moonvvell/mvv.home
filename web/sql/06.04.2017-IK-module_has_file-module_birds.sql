CREATE TABLE `module_has_file` (
  `id`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_id`       INT UNSIGNED NULL,
  `element_id`    INT UNSIGNED NULL,
  `element_table` VARCHAR(45)  NULL,
  `role_id`       INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idmodule_has_file_UNIQUE` (`id` ASC),
  INDEX `fk_module_to_file_idx` (`file_id` ASC),
  INDEX `fk_module_has_to_role_idx` (`role_id` ASC),
  CONSTRAINT `fk_module_to_file`
  FOREIGN KEY (`file_id`)
  REFERENCES `file` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_module_has_to_role`
  FOREIGN KEY (`role_id`)
  REFERENCES `roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


CREATE TABLE `notes` (
  `id`       INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `text`     TEXT         NULL,
  `add_date` DATETIME     NULL     DEFAULT CURRENT_TIMESTAMP,
  `order`    INT          NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
);

CREATE TABLE `module_birds` (
  `id`          INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `latin_name`  VARCHAR(350) NULL,
  `common_name` VARCHAR(350) NULL,
  `seen_place`  TEXT         NULL,
  `seen_date`   DATETIME     NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `latin_name_UNIQUE` (`latin_name` ASC)
);

CREATE TABLE `module_birds_has_note` (
  `id`        INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_module` INT UNSIGNED NULL,
  `id_note`   INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_module_birds_to_note_idx` (`id_module` ASC),
  INDEX `fk_module_note_to_note_idx` (`id_note` ASC),
  CONSTRAINT `fk_module_birds_to_note`
  FOREIGN KEY (`id_module`)
  REFERENCES `module_birds` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_module_note_to_note`
  FOREIGN KEY (`id_note`)
  REFERENCES `notes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
