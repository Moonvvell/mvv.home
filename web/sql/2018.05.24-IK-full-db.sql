-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: moonvvell.home
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorized_users`
--

DROP TABLE IF EXISTS `authorized_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorized_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(45) NOT NULL,
  `expired` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorized_users`
--

LOCK TABLES `authorized_users` WRITE;
/*!40000 ALTER TABLE `authorized_users` DISABLE KEYS */;
INSERT INTO `authorized_users` VALUES (1,1,'37755b1c769667404d96055b0468d5e4809f29e4','2016-06-30 20:31:30'),(2,2,'1320fe9d7a43b506fa7b3704af77bed6fcc36c0b','2016-07-16 22:03:47'),(3,1,'f5fd82ac3634dc3ef79c8fc3ca81497cf3d33ddc','2016-07-16 22:04:28'),(4,1,'623a2ea5635d9c10424ce51920d26d7cc6748f1c','2016-07-23 09:17:37'),(5,1,'1a02afbd4745e55e98f5335be875b53cf616e8c7','2016-12-15 20:35:59'),(6,1,'f20003cfcee57a760b5cd9402e5e075d5804c86e','2016-12-15 20:36:40'),(7,1,'d15f5ce9371c2913ad910e65563b7f292d6cd713','2016-12-15 21:39:12'),(8,4,'81015557b67f337395fa57887ed746704a808a81','2016-12-15 21:40:58'),(9,4,'9bad3b076c4a50d4744728768877abb4771436cd','2016-12-15 21:45:56'),(10,4,'56490a3d62badbfb8c01c589bdc7ae7c46449de7','2016-12-15 21:54:42'),(11,4,'16f4f051c6a6de41685d458e99fdbaea9d50638b','2016-12-15 21:58:11'),(12,4,'3b0739c35c25739de4dbbd024be4db02514015f1','2016-12-15 21:59:40'),(13,4,'95937934a53a615c2115b6eb8e460ee3b8873f27','2016-12-15 22:00:25'),(14,4,'268ee123acce1e1858e53da25ec20f6c7fc8431c','2016-12-15 22:02:12'),(15,4,'54f9f2d4130ff91cb9c66eca41d97e0969c3aa29','2016-12-15 22:06:19'),(16,4,'5ff359e2e91a7c12039c795b11cef39604c46bc5','2016-12-15 23:03:57'),(17,4,'85d7ae112a9fdfa824f2b4930d89593d2c4e1f79','2016-12-15 23:09:08'),(18,4,'695427d4fe7936b0eb0c56e4a881804379e34229','2016-12-15 23:09:34'),(19,4,'aec916c38475c2f7180be1e66f016be9626a70ec','2016-12-15 23:54:35'),(20,4,'c23f52b9350a0e4afae1a4a34c194e84fa626fe0','2016-12-16 00:29:57'),(21,4,'c7e8f6cbaa46927e46e59a64d034ae6d64fe9577','2016-12-16 09:06:08'),(22,4,'c7b4a5c7b9e7485f90c500e3e7e390009d0e60f8','2016-12-16 10:51:37'),(23,4,'325202b0011aea1fc9cbbaa06dc2d088c223665b','2016-12-16 11:46:09'),(24,4,'bde204d1b137cecdb825866f94df5e16da5a6e9a','2016-12-16 19:51:37'),(25,4,'4fca3595e8add756562fc60abe0b3123c28fa0c5','2016-12-16 19:53:06'),(26,4,'f68bedf4ab077c65d7a2d665b3f71a4ad036d787','2016-12-16 20:52:47'),(27,4,'b4b6350e434f50b01cc18ea8efdcbe3252db151c','2016-12-16 20:53:32'),(28,4,'2a7053aa766917339d37a6909a67a13e1fce6a0b','2016-12-17 10:54:52'),(29,4,'178986c054221a19805eb5ae995639fecd660a1d','2016-12-17 11:58:07'),(30,4,'8453229f8de6c6714e51147e2f18faf0d2d1f3aa','2016-12-17 14:42:36'),(31,4,'38b40d375439953af6368758338f4737448e8218','2016-12-23 22:23:32'),(32,4,'f0ee3bc4375ec390a305f9a5d6278ded1ba6190b','2016-12-23 22:23:32'),(33,4,'b680d176eef797ac3afe4a72b563314ea9820144','2016-12-23 23:05:38'),(34,4,'659676692781dde21d29d17fd9eddcd01e77f844','2016-12-23 23:06:45'),(35,4,'7ceec854ed7031b2bc0058983da427399505d548','2016-12-23 23:06:50'),(36,4,'4c42799cabb34bab0e286d4a2f4c2380b1bd0b2a','2016-12-23 23:53:12'),(37,4,'c845e42c6b65882a65b2fb6c577ad5ce7758eb63','2016-12-24 00:21:40'),(38,4,'e0fb5185c41c203cc874829d44857e309f0800ac','2016-12-24 00:28:14'),(39,4,'dcf25a91879a5df088f782f3ba677e05c5f0da95','2016-12-24 00:30:25'),(40,4,'5c826edd9f35b05896197165fe8ac3c70761261b','2016-12-24 12:25:09'),(41,4,'3f43c9fa82f6a09e22f2b5bb96bd6393c632e99f','2016-12-24 13:11:17'),(42,4,'e568c2f49eee23dc9748f65413c632a760e0521f','2016-12-24 13:19:14'),(43,4,'999d8469966036a5e72e1af71f5429493f146314','2016-12-24 16:26:48'),(44,4,'9388471926d9eece8fc2d807b5a29d2e44c82499','2016-12-24 16:47:58'),(45,4,'9310a256e6e4810d3671da0ed7c75391fe3dafd6','2016-12-24 17:10:21'),(46,4,'42da004b721cbf4cf7e8ed4aee2af9bd95206594','2016-12-24 18:36:29'),(47,4,'e3c98b93ae571b3bb4c45c937a2187306fd7d667','2016-12-24 19:22:11'),(48,4,'c2ea221fc74d2139854dda898b1149293e084922','2016-12-24 19:22:11'),(49,4,'fecda8951b5d36973d2d6cd8f841d1dea698faa1','2016-12-24 19:22:15'),(50,4,'5c29fcfa0a0794761416475636edc62a869c8377','2016-12-24 20:30:30'),(51,4,'5c6e4a3d7b42ee7b1799f062c8bd86bd20846a05','2016-12-24 22:45:22'),(52,4,'fa8cfbde23692d6d04de7476c08423143291f62d','2016-12-24 23:38:59'),(53,4,'f26921d71d2f486df5f28ee63a0e4dc47b174edf','2016-12-25 00:24:21'),(54,4,'0b7a7950f8c83283f2af5d69d19c7164d11f154d','2016-12-25 02:01:49'),(55,4,'bbe43647a4a4733148c2fa367194730e319ef9d2','2016-12-25 12:51:41'),(56,4,'ceb4f97107e4ab9aa919e2404ace322c01d623bf','2016-12-25 13:40:16'),(57,4,'4c83853f28691526912a3e0d0496ebc35a99c98e','2016-12-27 20:17:49'),(58,4,'9427f61e7e4626f29454d33f13d75ea2412c6c22','2016-12-27 20:17:49'),(59,4,'e7b95c925a1e72a1a1190ec95210f9e1e78d03d0','2016-12-27 20:17:49'),(60,4,'33cce2e48277c1584816337c2d6ea52ecb397902','2016-12-27 20:59:53'),(61,4,'466b839e93044c4e02ed8e1d69a4a9b3873bba7d','2016-12-27 21:03:20'),(62,4,'613bee6b4f316fd3550978c3f1691e8ec97ef3c8','2016-12-27 23:18:37'),(63,4,'cda0a3ac025e077a30231f3172c504b3dae81060','2016-12-27 23:18:45'),(64,4,'f9267084773c8d6ac7c65b749adfed307f7083ab','2016-12-28 13:21:50'),(65,4,'0324866965b3eaed67514ee349942884e6b20ff8','2016-12-28 14:42:45'),(66,4,'bfb6fc3000192b2766ddccce1fe525cf6947f774','2016-12-28 17:30:52'),(67,4,'e48b921b81eaded9d2495647f9f12b9f3395227f','2016-12-28 17:30:52'),(68,4,'985d6a905702310c95bf92a45470576abfa1d220','2017-01-03 22:32:54'),(69,4,'5b92b7ca06445f17b63098e70111f0cb26d333da','2017-01-03 22:32:54'),(70,4,'1d8db96ecb4b145261bea07cbd8dca2d5e59cedf','2017-01-03 22:34:11'),(71,4,'3f3a0ed4860604cf85459897ee2f3e4cca78af8d','2017-01-06 14:56:41'),(72,4,'757ae8450a1c5c293796f54cb0c2f22f018a9611','2017-01-06 14:56:41'),(73,4,'5436928ce424238c7998d3d21aa6738af374cc64','2017-01-06 16:12:24'),(74,4,'1c8d5dcfde595d1547becb4907b2984b59a51e8a','2017-01-06 16:37:34'),(75,4,'4b355d3afee359f09a6b2809ddf8547c541c3101','2017-01-06 19:15:45'),(76,4,'917935ba2a42e603d87e8c974c548671b357291c','2017-01-07 17:51:01'),(77,4,'1fe1ab3dee8479a54b4db963e5f3304da0c014cc','2017-01-07 20:49:04'),(78,4,'9c4299961cefbfbd0b6cc1160b1fad3dac5fce39','2017-01-28 15:32:29'),(79,4,'0caae3c2e6e6b48bc4dda5ea7c2ca6fbb33f92ad','2017-01-28 15:33:05'),(80,4,'738354acca90e548d2ee4f7ce1557e54acb0f89a','2017-01-28 15:34:42'),(81,4,'b2bc28e02770335edfb5bb8e8b56d148f541026b','2017-01-28 15:44:04'),(82,4,'27af5b73fdebf4a2765cdad9d45beb38ef6c507b','2017-02-11 16:15:01'),(83,4,'ae736c28e2c793b7ef95d6279661172c50841f14','2017-02-11 18:07:14'),(84,4,'e6607bd72a3128207361469f91cc038c1724b61c','2017-02-11 18:09:55'),(85,4,'4baee810f918d1f9b325199cc9df70dff60e43ca','2017-02-11 18:11:00'),(86,4,'393cca9ac619c68f039d9b105bb1cbbe6136be58','2017-02-11 18:55:50'),(87,4,'797bc71755112feefe5a4ecaf3e7e9ba4af87852','2017-02-11 19:40:53'),(88,4,'e1748208d1913015252a5fe211d6b20ceca1f3ca','2017-02-11 21:18:57'),(89,4,'34ad4c0f81fa6368922be73cb64a9dfb0c66ba88','2017-02-12 19:42:24'),(90,4,'bda497c906c80da5b5ff007699ea40473096e24a','2017-02-12 21:40:14'),(91,4,'668fabb21affad62ca54dc779a2a6f6bd0122ac1','2017-02-12 22:44:35'),(92,4,'1e7f6056fb2a4da916627649716eaa5c43f47081','2017-02-13 18:37:16'),(93,4,'83b7474caee88450fb65ce9f9048249bbf9d8b81','2017-02-13 19:22:25'),(94,4,'1761a7cd6ded93e949d9b07d80c0f1f070a52b50','2017-02-13 19:22:25'),(95,4,'925e88c8c490d59c1c7be8e03041bfe2168ae298','2017-02-13 20:10:59'),(96,4,'0cb5f4a87da633d8aaa60859591d15f055c2cfe4','2017-02-13 20:10:59'),(97,4,'bff8d981063e617ac3169e39c352ee12fccfec85','2017-02-13 22:04:40'),(98,4,'72a1b8683cdc3cb0fffb188d2a4431af1d3d2954','2017-02-13 22:50:57'),(99,4,'0428cfe8c3493b60de81bf66511eeb30e62e6115','2017-02-14 09:26:02'),(100,4,'2d5822b7782b5d556a13c9ecbce5d635a74fa869','2017-02-15 18:28:55'),(101,4,'ebe19f78209175625d3aa90bafb09660c6d6d6b2','2017-02-15 19:14:11'),(102,4,'a9d10aa1ba520b650b482c0f392e8bbe146216cb','2017-02-15 19:14:15'),(103,4,'167a2d916adb3f3d6fc788e227db75c16d06d39e','2017-02-15 19:14:45'),(104,4,'25a5b8ca6b4c844de0c2b0ee441e681f89b050d9','2017-02-15 19:59:32'),(105,4,'3430ad29b01c54e9cb9b7072ea12b88ea980f4bd','2017-02-15 21:31:32'),(106,4,'b0092e99b45029e174684bbe7a233a6e90b12152','2017-02-15 21:53:55'),(107,4,'6092debb943298c58c4eddc16fe94327ee160d1e','2017-02-15 22:22:06'),(108,4,'986b09fda6655fdc0893edc9ca8ea239da157e37','2017-02-15 22:22:06'),(109,4,'a61fe577846298dcb7b159231223814f12c4705b','2017-02-15 23:07:09'),(110,4,'b032dad6a1a5b6bcb0c0024f17ed88d311ba61c9','2017-02-16 07:37:48'),(111,4,'bd94326ef3972884fbb2e5196870e38b32868441','2017-02-16 08:49:19'),(112,4,'9c4ad0477fd54b78ea2dec1637fa9cb0ac96e2ef','2017-02-16 18:37:56'),(113,4,'182a116c0291f4bc92e9d5a954412b0f4da35bfa','2017-02-17 20:32:25'),(114,4,'2fc9dd15fca0f95788a7523286c45162af755646','2017-02-18 15:25:07'),(115,4,'2464645ec648b13638b636d24c2cf272faaf125c','2017-02-19 18:41:32'),(116,4,'c60e85244374d48bf9430ea761ba790adf7688c6','2017-02-26 11:40:19'),(117,4,'2c9abbfc48fdca32653fb20d5a56d7b37cc7714b','2017-02-26 11:40:19'),(118,4,'5e56f6fc233a1017f4c3772a5dfb5638fd52dd28','2017-02-26 12:39:30'),(119,4,'95919bc58494deec529bd21d4465def06371b4c4','2017-03-05 22:02:54'),(120,4,'7ecf4cd160b75201b808181558aa8244ab5da01d','2017-03-05 22:02:54'),(121,4,'045b12277573acef8337f95fa6ad0612d45748d0','2017-03-07 20:00:27'),(122,4,'14de35a9a65a3291e0f8f2741d3b73d219e09e89','2017-03-07 20:00:27'),(123,4,'1e6f42c78948f3997d49e3fe0a557189ab731e46','2017-03-07 20:50:00'),(124,4,'021cb8cb403e2795f05867e1faf6214e4609d293','2017-03-07 20:50:00'),(125,4,'cea64607dc00d76db1475bbd1f7c1b8bfada3a47','2017-03-07 21:36:46'),(126,4,'9823823aba57274ea550a9ec98781fb90e23d3dc','2017-03-07 21:36:46'),(127,4,'50c6558da8c01f559f1d5cbd4f214abd90043d38','2017-03-07 22:30:18'),(128,4,'0ab9cabe27b4013150895e49dcb9d7f6594df9bc','2017-03-07 22:30:18'),(129,4,'3531284bfb6e903c42d3133acff1f20776e040fe','2017-03-08 08:16:39'),(130,4,'5649b1f790c6e2fd69fa9c2cb91619aacca8fb73','2017-03-08 17:28:31'),(131,4,'98e3041b076f48d93264b8f60d04b610ed5d6779','2017-03-08 17:40:15'),(132,4,'71c079124a36c554bb312e3f9ed6b76853e45316','2017-03-08 18:45:36'),(133,4,'51eb075e69d768de0e0f9b03c60e23080be2c602','2017-03-08 19:31:53'),(134,4,'82da67de0fdf91a2f7059213416a6c3756449875','2017-03-08 19:31:53'),(135,4,'93c8069407c21377342fe960c7de047ef97a4be7','2017-03-08 20:22:53'),(136,4,'6ee884343dcaf5603631a46a277f7e0dafdd8fae','2017-03-08 20:22:53'),(137,4,'6408bd4ce94d30d2f7558c2c88d9fa8241e90e46','2017-03-09 08:09:07'),(138,4,'59f4d092fb495bc393c9b63baf724fba03113ffe','2017-03-09 18:24:37'),(139,4,'04ad2bee5c7458ba94287465eb5a3110a27c592d','2017-03-10 20:05:22'),(140,4,'9b87c110c1d6ba392c8523f90985242c74893360','2017-03-10 20:05:22'),(141,4,'fb12836bacb25669a48a67fbc8af11c55b3b85eb','2017-03-11 11:50:46'),(142,4,'d8f1ed45f3c34882a8d9db87f0537ae22c3048a5','2017-03-13 19:26:04'),(143,4,'94ea3fc42d1fb1a98d654a7dea778d01ef1405d6','2017-03-14 23:15:29'),(144,4,'bd868b508bec8178454630abff1d7ecf01da61e3','2017-03-15 00:20:02'),(145,4,'3d964b47d8b4c55ff0cc09f56722563c1d181b37','2017-03-18 11:57:54'),(146,4,'b12d5cc2bd3d2b000c9ccd1a8a8058130e96b3aa','2017-03-18 12:10:07'),(147,5,'fe4e8f8fffaf4baf92f1095d7155fcbd51b18306','2017-03-18 12:27:00'),(148,4,'3c26c222c6efcd070aeada1f542ffce4ef084f7e','2017-03-18 12:27:05'),(149,4,'58a1ee0666e64743ec9f07e5e3905c5159fa9765','2017-03-18 12:27:52'),(150,4,'2aa88625b1b2ada291ed5bed63e8c8d1d1f894fa','2017-03-18 12:31:29'),(151,4,'20de97cae5888ce050f60059470263501a386371','2017-03-18 12:33:01'),(152,4,'b25ff1b31eab2e8eaa9226ea752b2945b230ae9d','2017-03-18 12:34:51'),(153,4,'3f522a9a152c54da96eaf5fbc22a6ff983795c97','2017-03-18 12:36:27'),(154,4,'7349e297d250f26a4e4a6c35e191396828379d73','2017-03-18 17:52:53'),(155,4,'cb3cd4ab79f1b91d6b25435d3cdc2fba339f9a29','2017-03-18 17:55:45'),(156,4,'58f386c247935f1af5c704f11f3425fda4ffbb0f','2017-03-18 19:02:04'),(157,4,'6f87e24bc22cdb7d2ff5db70a881b19fff0e1550','2017-03-18 19:13:47'),(158,4,'35ade24c725bddcf6d129028603ded79a44564c8','2017-03-18 19:32:17'),(159,4,'a18e89788577d2d46ff1fba28884ef628289d732','2017-03-18 19:37:19'),(160,4,'29f9e1796510d29201d34f99569cc9bc2ef4e5b8','2017-03-18 20:22:20'),(161,4,'afacad7e466ed1c26b402cb0fb10ce1e80854a92','2017-03-18 20:22:20'),(162,4,'41d727e9d61fa62e9b91375930265bca623828ee','2017-03-19 09:06:11'),(163,4,'da93f81b91bc4622ba73e198ea4c6e7794711f6e','2017-03-23 08:08:41'),(164,4,'4a520a371339637e8d126e7f4c0e6436d27f1d0a','2017-03-24 19:35:42');
/*!40000 ALTER TABLE `authorized_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(45) NOT NULL,
  `file_path` varchar(45) DEFAULT NULL,
  `file_extension` varchar(45) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `element_table` varchar(45) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `original_name` text,
  `add_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `file_mime` varchar(45) DEFAULT NULL,
  `file_comment` text,
  PRIMARY KEY (`id`),
  KEY `fk_file_to_user_idx` (`user_id`),
  CONSTRAINT `fk_file_to_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` VALUES (28,'84e6312b6f','uploads/','png',1,4,'user',4,'0005','2018-05-03 13:43:43','image/png',NULL),(29,'f1a210ebfd','uploads/','png',2,4,'user',4,'0013','2018-05-03 14:08:16','image/png',NULL),(30,'4650c58ab9','uploads/','png',2,4,'user',4,'0002','2018-05-03 14:09:19','image/png',NULL),(31,'e20eb72c5b','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:12:31','image/png',NULL),(32,'672e67a1ea','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:14:11','image/png',NULL),(33,'e8033fa6d7','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:15:10','image/png',NULL),(34,'8082748375','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:15:13','image/png',NULL),(35,'330f2eb7ac','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:16:00','image/png',NULL),(36,'ad7a346506','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:16:27','image/png',NULL),(37,'63c3df018d','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:16:46','image/png',NULL),(38,'48e5771dfc','uploads/','png',2,4,'insects_collection',4,'0002','2018-05-03 14:17:09','image/png',NULL),(39,'0ac7a2381c','uploads/','png',2,8,'insects_collection',4,'0002','2018-05-03 14:18:30','image/png',NULL),(40,'a074438135','uploads/','png',2,8,'insects_collection',4,'0002','2018-05-03 14:20:20','image/png',NULL),(41,'0afecfbb97','uploads/','png',2,8,'insects_collection',4,'0002','2018-05-03 14:20:38','image/png',NULL),(42,'080ad92eb8','uploads/','png',2,8,'insects_collection',4,'0018','2018-05-03 14:51:40','image/png',NULL),(43,'6edc5318c2','uploads/','png',2,8,'insects_collection',4,'0004','2018-05-08 17:53:40','image/png',NULL),(44,'543b370105','uploads/','png',2,8,'insects_collection',4,'0017','2018-05-08 18:18:58','image/png',NULL),(45,'a3f03e7422','uploads/','jpg',2,8,'insects_collection',4,'0022','2018-05-08 18:23:46','image/jpeg',NULL),(46,'6140f08aba','uploads/','jpg',2,40,'insects_collection',4,'photo_2018-05-08_18-56-23','2018-05-08 19:09:02','image/jpeg',NULL),(47,'f0f604883e','uploads/','jpg',2,42,'insects_collection',4,'photo_2018-05-08_19-13-29','2018-05-08 19:16:10','image/jpeg',NULL),(48,'a0406841dc','uploads/','jpg',2,43,'insects_collection',4,'photo_2018-05-08_19-18-49','2018-05-08 19:20:01','image/jpeg',NULL),(49,'78171b9723','uploads/','jpg',2,44,'insects_collection',4,'photo_2018-05-08_19-21-43','2018-05-08 19:22:50','image/jpeg',NULL),(50,'3913078e0d','uploads/','jpg',2,45,'insects_collection',4,'photo_2018-05-08_19-35-33','2018-05-08 19:35:37','image/jpeg',NULL);
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insects_collection`
--

DROP TABLE IF EXISTS `insects_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insects_collection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catch_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `catch_user_name` varchar(255) DEFAULT NULL,
  `catch_country` varchar(255) DEFAULT NULL,
  `catch_place` varchar(255) DEFAULT NULL,
  `catch_place_description` text,
  `catch_place_coordinates` varchar(100) DEFAULT NULL,
  `definition_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `definition_user_name` varchar(255) DEFAULT NULL,
  `latin_name` text,
  `other_name` text,
  `sex` tinyint(1) DEFAULT NULL,
  `identificator` text,
  `collection_box` text,
  `add_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `insects_collection_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='Всі комахи в моїй колекції';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insects_collection`
--

LOCK TABLES `insects_collection` WRITE;
/*!40000 ALTER TABLE `insects_collection` DISABLE KEYS */;
INSERT INTO `insects_collection` VALUES (8,'2018-05-08 16:23:46','Ihor Kyryliuk','Wroclaw','Near river. Ot the railings','На перилах біля Одри. Під деревами, як йти з парку Шітніцького до плацу Грюнвальдського','51.111409, 17.070100','2018-04-03 17:36:36','','','',0,'03Apr-fbe2','1','2018-04-03 17:36:36','2018-05-08 16:23:46'),(9,'2018-04-03 18:28:50','Ihor Kyryliuk','Wroclaw','Near river. Ot the railings','На перилах біля Одри. Під деревами, як йти з парку Шітніцького до плацу Грюнвальдського','51.111409, 17.070100','2018-04-03 17:59:13','','','',0,'03Apr-e70c','1','2018-04-03 17:59:13','2018-04-03 18:28:50'),(10,'2018-04-03 18:28:47','Ihor Kyryliuk','Wroclaw','Near river. Ot the railings','На перилах біля Одри. Під деревами, як йти з парку Шітніцького до плацу Грюнвальдського','51.111409, 17.070100','2018-04-03 17:59:31','','','',0,'03Apr-7c3c','1','2018-04-03 17:59:31','2018-04-03 18:28:47'),(11,'2018-05-03 12:08:16','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:30:19','','','',0,'25Apr-654f','-','2018-04-25 17:30:19','2018-05-03 12:08:16'),(13,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:33:58','','','',0,'25Apr-efbd','-','2018-04-25 17:33:58','2018-04-25 17:33:58'),(14,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:34:30','','','',0,'25Apr-e643','-','2018-04-25 17:34:30','2018-04-25 17:34:30'),(15,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:34:45','','','',0,'25Apr-b02a','-','2018-04-25 17:34:45','2018-04-25 17:34:45'),(16,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:34:59','','','',0,'25Apr-eae5','-','2018-04-25 17:34:59','2018-04-25 17:34:59'),(17,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:14','','','',0,'25Apr-c24f','-','2018-04-25 17:35:14','2018-04-25 17:35:14'),(18,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-95f5','-','2018-04-25 17:35:26','2018-04-25 17:35:26'),(19,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:36:04','','','',0,'25Apr-4bbe','-','2018-04-25 17:36:04','2018-04-25 17:36:04'),(21,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:36:35','','','',0,'25Apr-3941','-','2018-04-25 17:36:35','2018-04-25 17:36:35'),(22,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:37:27','','','',0,'25Apr-6eea','-','2018-04-25 17:37:27','2018-04-25 17:37:27'),(23,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:37:44','','','',0,'25Apr-f97b','-','2018-04-25 17:37:44','2018-04-25 17:37:44'),(24,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:37:57','','','',0,'25Apr-9635','-','2018-04-25 17:37:57','2018-04-25 17:37:57'),(25,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:39:05','','','',0,'25Apr-45b2','-','2018-04-25 17:39:05','2018-04-25 17:39:05'),(26,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:40:00','','','',0,'25Apr-6cdd','-','2018-04-25 17:40:00','2018-04-25 17:40:00'),(27,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:45:18','','','',0,'25Apr-4800','-','2018-04-25 17:45:18','2018-04-25 17:45:18'),(28,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:45:29','','','',0,'25Apr-bc06','-','2018-04-25 17:45:29','2018-04-25 17:45:29'),(29,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:45:29','','','',0,'25Apr-462d','-','0000-00-00 00:00:00','2018-04-25 18:00:12'),(30,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:45:29','','','',0,'25Apr-51ce','-','0000-00-00 00:00:00','2018-04-25 18:00:15'),(31,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:45:29','','','',0,'25Apr-6567','-','0000-00-00 00:00:00','2018-04-25 18:00:17'),(32,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:36:20','','','',0,'25Apr-addd','-','0000-00-00 00:00:00','2018-04-25 18:00:25'),(33,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-3bf0','-','0000-00-00 00:00:00','2018-04-25 18:00:26'),(34,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-d951','-','0000-00-00 00:00:00','2018-04-25 18:00:27'),(35,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-1c20','-','0000-00-00 00:00:00','2018-04-25 18:00:28'),(36,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-dab7','-','0000-00-00 00:00:00','2018-04-25 18:00:30'),(37,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-9935','-','0000-00-00 00:00:00','2018-04-25 18:00:31'),(38,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-fbc1','-','0000-00-00 00:00:00','2018-04-25 18:00:36'),(39,'2018-04-14 22:00:00','Ihor Kyryliuk','Ukraine','Kyiv oblast, Lutizh village','Вздовж Дніпра на піщаному березі','50.725174, 30.380721','2018-04-25 17:35:26','','','',0,'25Apr-c588','-','0000-00-00 00:00:00','2018-04-25 18:00:37'),(40,'2016-05-24 22:00:00','Ihor Kyryliuk','Georgia','Svaneti, Mestia','In the grass','46.046, 42.892','2018-05-07 22:00:00','Kyryliuk I.O.','Staphylinus caesareus Cederhjelm, 1798','',0,'08May-1e32','2','2018-05-08 16:51:08','2018-05-08 17:29:13'),(41,'2017-05-20 22:00:00','Ihor Kyryliuk','Poland','Wroclaw. Strachocinski forest','-','51.098, 17.145','2018-05-07 22:00:00','Kyryliuk I.O.','Valgus hemipterus','',0,'08May-6935','1','2018-05-08 17:15:03','2018-05-08 17:15:03'),(42,'2016-05-24 22:00:00','Ihor Kyryliuk','Georgia','Svaneti, Mestia','-','43.046, 42.892','2018-05-07 22:00:00','Kyryliuk I.O.','Valgus hemipterus','',0,'08May-9d53','1','0000-00-00 00:00:00','2018-05-08 17:30:58'),(43,'2013-12-31 23:00:00','Ihor Kyryliuk','Ukraine','-','-','-','2018-05-07 22:00:00','Kyryliuk I.O.','Protaetia affinis','-',0,'08May-0f07','2','2018-05-08 17:19:26','2018-05-08 17:29:37'),(44,'2013-12-31 23:00:00','Ihor Kyryliuk','Ukraine','-','-','-','2018-05-07 22:00:00','Kyryliuk I.O.','Dorcadion holosericeum','Вусач-коренеїд посмугований',0,'08May-175b','-','2018-05-08 17:22:34','2018-05-08 17:29:31'),(45,'2013-12-31 23:00:00','Ihor Kyryliuk','Ukraine','-','-','-','2018-05-07 22:00:00','Kyryliuk I.O.','Prionus coriarius','Вусач-шкіряник лісовий',2,'08May-bbcc','-','2018-05-08 17:35:28','2018-05-08 17:35:37');
/*!40000 ALTER TABLE `insects_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insects_collection_has_files`
--

DROP TABLE IF EXISTS `insects_collection_has_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insects_collection_has_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ichf_to_ic_idx` (`file_id`),
  KEY `fk_ichf_to_ic_idx1` (`item_id`),
  CONSTRAINT `fk_ichf_to_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ichf_to_ic` FOREIGN KEY (`item_id`) REFERENCES `insects_collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insects_collection_has_files`
--

LOCK TABLES `insects_collection_has_files` WRITE;
/*!40000 ALTER TABLE `insects_collection_has_files` DISABLE KEYS */;
INSERT INTO `insects_collection_has_files` VALUES (8,40,46),(9,42,47),(10,43,48),(11,44,49),(12,45,50);
/*!40000 ALTER TABLE `insects_collection_has_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_birds`
--

DROP TABLE IF EXISTS `module_birds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_birds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `latin_name` varchar(350) DEFAULT NULL,
  `common_name` varchar(350) DEFAULT NULL,
  `seen_place` text,
  `seen_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `latin_name_UNIQUE` (`latin_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_birds`
--

LOCK TABLES `module_birds` WRITE;
/*!40000 ALTER TABLE `module_birds` DISABLE KEYS */;
INSERT INTO `module_birds` VALUES (1,'','','',NULL);
/*!40000 ALTER TABLE `module_birds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_birds_has_note`
--

DROP TABLE IF EXISTS `module_birds_has_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_birds_has_note` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_module` int(10) unsigned DEFAULT NULL,
  `id_note` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_module_birds_to_note_idx` (`id_module`),
  KEY `fk_module_note_to_note_idx` (`id_note`),
  CONSTRAINT `fk_module_birds_to_note` FOREIGN KEY (`id_module`) REFERENCES `module_birds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_module_note_to_note` FOREIGN KEY (`id_note`) REFERENCES `notes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_birds_has_note`
--

LOCK TABLES `module_birds_has_note` WRITE;
/*!40000 ALTER TABLE `module_birds_has_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_birds_has_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_contacts`
--

DROP TABLE IF EXISTS `module_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `father_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` int(2) DEFAULT NULL,
  `photo_id` int(10) unsigned DEFAULT NULL,
  `contact_group_id` int(10) unsigned DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_contacts_groups_idx` (`contact_group_id`),
  KEY `fk_contacts_photo_idx` (`photo_id`),
  CONSTRAINT `fk_contacts_groups` FOREIGN KEY (`contact_group_id`) REFERENCES `module_contacts_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_contacts_photo` FOREIGN KEY (`photo_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_contacts`
--

LOCK TABLES `module_contacts` WRITE;
/*!40000 ALTER TABLE `module_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_contacts_contacts`
--

DROP TABLE IF EXISTS `module_contacts_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_contacts_contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contact_id` int(10) unsigned DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `type` int(10) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_contact_idx` (`contact_id`),
  CONSTRAINT `fk_contact` FOREIGN KEY (`contact_id`) REFERENCES `module_contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_contacts_contacts`
--

LOCK TABLES `module_contacts_contacts` WRITE;
/*!40000 ALTER TABLE `module_contacts_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_contacts_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_contacts_groups`
--

DROP TABLE IF EXISTS `module_contacts_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_contacts_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `color` varchar(45) DEFAULT '#ffffff',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_contacts_groups`
--

LOCK TABLES `module_contacts_groups` WRITE;
/*!40000 ALTER TABLE `module_contacts_groups` DISABLE KEYS */;
INSERT INTO `module_contacts_groups` VALUES (1,'я','#935fa9'),(2,'сім\'я','#218898'),(3,'Друзі','#ffffff');
/*!40000 ALTER TABLE `module_contacts_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_has_file`
--

DROP TABLE IF EXISTS `module_has_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_has_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int(10) unsigned DEFAULT NULL,
  `element_id` int(10) unsigned DEFAULT NULL,
  `element_table` varchar(45) DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idmodule_has_file_UNIQUE` (`id`),
  KEY `fk_module_to_file_idx` (`file_id`),
  KEY `fk_module_has_to_role_idx` (`role_id`),
  CONSTRAINT `fk_module_has_to_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_module_to_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_has_file`
--

LOCK TABLES `module_has_file` WRITE;
/*!40000 ALTER TABLE `module_has_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_has_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text,
  `add_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `slug` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Avatar','ava','1'),(2,'Фото колекції','insects_collection_photo','Collection photo');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `color` varchar(45) DEFAULT '#ffffff',
  `rights` int(10) NOT NULL DEFAULT '0',
  `session_lifetime` int(11) NOT NULL DEFAULT '3600',
  `user_avatar_id` int(11) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','c3284d0f94606de1fd2af172aba15bf3','#46BECE',1,3600,3,'2016-07-16 19:03:37'),(2,'111','3049a1f0f1c808cdaa4fbed0e01649b1','#5B1CDF',3,3600,NULL,'2016-07-16 19:03:42'),(4,'Moonvvell','2ca18f2ce53f9d908907fc1ded2b6bd3','#25B12E',1,666666,5,'2016-12-15 19:40:47'),(5,'Moonvvell2','2ca18f2ce53f9d908907fc1ded2b6bd3','#ffffff',3,3600,NULL,'2017-03-18 10:08:41');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-24 20:19:32
