CREATE TABLE insects_collection
(
  id                      INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  catch_date              TIMESTAMP,
  catch_user_name         VARCHAR(255),
  catch_country           VARCHAR(255),
  catch_place             VARCHAR(255),
  catch_place_description TEXT,
  catch_place_coordinates VARCHAR(100),
  definition_date         TIMESTAMP,
  definition_user_name    VARCHAR(255),
  latin_name              TEXT,
  other_name              TEXT,
  sex                     TINYINT(1)               DEFAULT NULL,
  identificator           TEXT,
  collection_box          TEXT,
  add_date                TIMESTAMP       NOT NULL,
  last_change_date        TIMESTAMP                DEFAULT CURRENT_TIMESTAMP
);
CREATE UNIQUE INDEX insects_collection_id_uindex
  ON insects_collection (id);
ALTER TABLE insects_collection
  COMMENT = 'Всі комахи в моїй колекції';