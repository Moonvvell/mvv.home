<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "module_birds_has_note".
 *
 * @property string $id
 * @property string $id_module
 * @property string $id_note
 *
 * @property ModuleBirds $idModule
 * @property Notes $idNote
 */
class ModuleBirdsHasNote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_birds_has_note';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_module', 'id_note'], 'integer'],
            [['id_module'], 'exist', 'skipOnError' => true, 'targetClass' => ModuleBirds::className(), 'targetAttribute' => ['id_module' => 'id']],
            [['id_note'], 'exist', 'skipOnError' => true, 'targetClass' => Notes::className(), 'targetAttribute' => ['id_note' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_module' => Yii::t('app', 'Id Module'),
            'id_note' => Yii::t('app', 'Id Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModule()
    {
        return $this->hasOne(ModuleBirds::className(), ['id' => 'id_module']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNote()
    {
        return $this->hasOne(Notes::className(), ['id' => 'id_note']);
    }
}
