<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 31.03.2017
 * Time: 21:40
 */

namespace app\models;

use yii\base\Model;
use app\models\File;
use yii\db\Exception;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function generatePhotoName()
    {
        return substr(md5(time()) . md5(time()), 0, 10);
    }

    /**
     * @param $roleId
     * @param null $elementId
     * @param null $tableName
     * @return array|bool
     * @throws \yii\base\InvalidParamException
     */
    public function upload($roleId, $elementId = null, $tableName = null, $tableHasFileClass = null)
    {
        if ($this->validate()) {
            $photoName = $this->generatePhotoName();
            $file = new File();
            $file->setAttribute('file_name', $photoName);
            $file->setAttribute('original_name', $this->imageFile->baseName);
            $file->setAttribute('file_extension', $this->imageFile->extension);
            $file->setAttribute('file_mime', $this->imageFile->type);
            $file->setAttribute('user_id', \Yii::$app->user->id);
            $file->setAttribute('user_id', \Yii::$app->user->id);
            $file->setAttribute('file_path', 'uploads/');
            $file->setAttribute('role_id', $roleId);
            $file->setAttribute('element_id', $elementId);
            $file->setAttribute('element_table', $tableName);
            if ($file->save(false)) {
                $path = 'uploads/' . $photoName . '.' . $this->imageFile->extension;
                $this->imageFile->saveAs($path);
                if ($tableHasFileClass !== null) {
                    $this->createTableHasFileRecord($elementId, $file->id, $tableHasFileClass);
                }
                return ['fileId' => $file->id, 'filePath' => $path];
            }
            return false;
        }
        return false;
    }

    private function createTableHasFileRecord($recordId, $fileId, $className)
    {
        $createdClass = new $className();
        $createdClass->file_id = $fileId;
        $createdClass->item_id = $recordId;
        $createdClass->save();
        var_dump($createdClass);
    }
}
