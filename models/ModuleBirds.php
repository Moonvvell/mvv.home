<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "module_birds".
 *
 * @property string $id
 * @property string $latin_name
 * @property string $common_name
 * @property string $seen_place
 * @property string $seen_date
 *
 * @property ModuleBirdsHasNote[] $moduleBirdsHasNotes
 */
class ModuleBirds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_birds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seen_place'], 'string'],
            [['seen_date'], 'safe'],
            [['latin_name', 'common_name'], 'string', 'max' => 350],
            [['latin_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'latin_name' => Yii::t('app', 'Latin Name'),
            'common_name' => Yii::t('app', 'Common Name'),
            'seen_place' => Yii::t('app', 'Seen Place'),
            'seen_date' => Yii::t('app', 'Seen Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModuleBirdsHasNotes()
    {
        return $this->hasMany(ModuleBirdsHasNote::className(), ['id_module' => 'id']);
    }

    /**
     * @return string
     */
    public function getMainPhoto()
    {
        /** @var Roles $role */
        $role = Roles::find()->where('`slug` = "' . Roles::$mainPhoto . '"')->one();
        /** @var ModuleHasFile $mhf */
        $mhf = ModuleHasFile::find()->where('element_table = "' . self::tableName() . '" AND element_id = ' . $this->id . ' AND role_id = ' . $role->id)->one();
        if (!isset($mhf)) {
            return File::$noFilePng;
        }
        /** @var File $file */
        $file = $mhf->getFile()->one();
        return $file->getFullPath();
    }

    public function getFormattedTitle()
    {
        $return = '';
        $commonName = $this->common_name;
        $latinName = $this->latin_name;
        if ($commonName != '') {
            $return .= '<span class="common-name">' . $commonName . '</span>';
            if ($latinName != '')
                $return .= '<br>(<span class="latin-name">' . $latinName . '</span>)';
            return $return;
        }
        return '<span class="latin-name">' . $latinName . '</span>';
    }
}
