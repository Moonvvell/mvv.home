<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InsectsCollection;

/**
 * InsectsCollectionSearch represents the model behind the search form about `app\models\InsectsCollection`.
 */
class InsectsCollectionSearch extends InsectsCollection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sex'], 'integer'],
            [['catch_date', 'catch_user_name', 'catch_country', 'catch_place', 'catch_place_description', 'catch_place_coordinates', 'definition_date', 'definition_user_name', 'latin_name', 'other_name', 'identificator', 'collection_box', 'add_date', 'last_change_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InsectsCollection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'catch_date' => $this->catch_date,
            'definition_date' => $this->definition_date,
            'sex' => $this->sex,
            'add_date' => $this->add_date,
            'last_change_date' => $this->last_change_date,
        ]);

        $query->andFilterWhere(['like', 'catch_user_name', $this->catch_user_name])
            ->andFilterWhere(['like', 'catch_country', $this->catch_country])
            ->andFilterWhere(['like', 'catch_place', $this->catch_place])
            ->andFilterWhere(['like', 'catch_place_description', $this->catch_place_description])
            ->andFilterWhere(['like', 'catch_place_coordinates', $this->catch_place_coordinates])
            ->andFilterWhere(['like', 'definition_user_name', $this->definition_user_name])
            ->andFilterWhere(['like', 'latin_name', $this->latin_name])
            ->andFilterWhere(['like', 'other_name', $this->other_name])
            ->andFilterWhere(['like', 'identificator', $this->identificator])
            ->andFilterWhere(['like', 'collection_box', $this->collection_box]);

        return $dataProvider;
    }
}
