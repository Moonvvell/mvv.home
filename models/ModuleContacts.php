<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "module_contacts".
 *
 * @property string $id
 * @property string $name
 * @property string $father_name
 * @property string $last_name
 * @property string $birthday
 * @property integer $gender
 * @property string $photo_id
 * @property string $contact_group_id
 * @property string $comment
 *
 * @property ModuleContactsGroups $contactGroup
 * @property File $photo
 * @property ModuleContactsContacts[] $moduleContactsContacts
 */
class ModuleContacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['birthday'], 'safe'],
            [['gender', 'photo_id', 'contact_group_id'], 'integer'],
            [['name', 'father_name', 'last_name'], 'string', 'max' => 45],
            [['comment'], 'string', 'max' => 500],
            [['contact_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ModuleContactsGroups::className(), 'targetAttribute' => ['contact_group_id' => 'id']],
            [['photo_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['photo_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'father_name' => Yii::t('app', 'Father Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'birthday' => Yii::t('app', 'Birthday'),
            'gender' => Yii::t('app', 'Gender'),
            'photo_id' => Yii::t('app', 'Photo ID'),
            'contact_group_id' => Yii::t('app', 'Contact Group ID'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactGroup()
    {
        return $this->hasOne(ModuleContactsGroups::className(), ['id' => 'contact_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(File::className(), ['id' => 'photo_id']);
    }

    public function getPhotoAddress()
    {
        /** @var File $file */
        $file = $this->getPhoto()->one();
        $link = isset($file) ? $file->getFullPath() : File::$noFilePng;
        return $link;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModuleContactsContacts()
    {
        return $this->hasMany(ModuleContactsContacts::className(), ['contact_id' => 'id']);
    }
}
