<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "module_contacts_contacts".
 *
 * @property string $id
 * @property string $contact_id
 * @property string $value
 * @property integer $type
 *
 * @property ModuleContacts $contact
 */
class ModuleContactsContacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_contacts_contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'type'], 'integer'],
            [['value'], 'string', 'max' => 45],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => ModuleContacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact ID'),
            'value' => Yii::t('app', 'Value'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(ModuleContacts::className(), ['id' => 'contact_id']);
    }
}
