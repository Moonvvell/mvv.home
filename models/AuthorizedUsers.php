<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authorized_users".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $token
 * @property string $expired
 */
class AuthorizedUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authorized_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['token'], 'required'],
            [['expired'], 'safe'],
            [['token'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'token' => Yii::t('app', 'Token'),
            'expired' => Yii::t('app', 'Expired'),
        ];
    }
}
