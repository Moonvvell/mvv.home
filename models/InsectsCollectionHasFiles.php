<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insects_collection_has_files".
 *
 * @property string $id
 * @property string $item_id
 * @property string $file_id
 *
 * @property File $file
 * @property InsectsCollection $item
 */
class InsectsCollectionHasFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insects_collection_has_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'file_id'], 'required'],
            [['item_id', 'file_id'], 'integer'],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InsectsCollection::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'file_id' => 'File ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(InsectsCollection::className(), ['id' => 'item_id']);
    }
}
