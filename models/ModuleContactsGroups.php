<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "module_contacts_groups".
 *
 * @property string $id
 * @property string $name
 * @property string $color
 *
 * @property ModuleContacts[] $moduleContacts
 */
class ModuleContactsGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_contacts_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'color'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'color' => Yii::t('app', 'Color'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModuleContacts()
    {
        return $this->hasMany(ModuleContacts::className(), ['contact_group_id' => 'id']);
    }
}
