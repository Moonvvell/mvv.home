<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notes".
 *
 * @property string $id
 * @property string $text
 * @property string $add_date
 * @property integer $order
 *
 * @property ModuleBirdsHasNote[] $moduleBirdsHasNotes
 */
class Notes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['add_date'], 'safe'],
            [['order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'add_date' => Yii::t('app', 'Add Date'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModuleBirdsHasNotes()
    {
        return $this->hasMany(ModuleBirdsHasNote::className(), ['id_note' => 'id']);
    }
}
