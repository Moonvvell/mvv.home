<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property string $id
 * @property string $file_name
 * @property string $original_name
 * @property string $file_extension
 * @property string $file_mime
 * @property string $user_id
 * @property string $file_path
 * @property string $file_comment
 * @property string $role_id
 * @property integer $element_id
 * @property string $element_table
 * @property string $add_date
 *
 * @property Roles $role
 * @property User $user
 * @property ModuleContacts[] $moduleContacts
 */
class File extends \yii\db\ActiveRecord
{
    static $noFilePng = 'media/pictures/nofile.png';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'file_extension', 'file_mime'], 'required'],
            [['user_id', 'role_id', 'element_id'], 'integer'],
            [['add_date'], 'safe'],
            [['file_name', 'original_name', 'file_extension', 'file_mime', 'file_path', 'element_table'], 'string', 'max' => 255],
            [['file_comment'], 'string', 'max' => 255],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'file_name' => Yii::t('app', 'File Name'),
            'original_name' => Yii::t('app', 'Original Name'),
            'file_extension' => Yii::t('app', 'File Extension'),
            'file_mime' => Yii::t('app', 'File Mime'),
            'user_id' => Yii::t('app', 'User ID'),
            'file_path' => Yii::t('app', 'File Path'),
            'file_comment' => Yii::t('app', 'File Comment'),
            'role_id' => Yii::t('app', 'Role ID'),
            'element_id' => Yii::t('app', 'Element ID'),
            'element_table' => Yii::t('app', 'Element Table'),
            'add_date' => Yii::t('app', 'Add Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Roles::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModuleContacts()
    {
        return $this->hasMany(ModuleContacts::className(), ['photo_id' => 'id']);
    }

    public function getFullPath()
    {
        $path = $this->file_path . $this->file_name . '.' . $this->file_extension;
        return file_exists($path) ? $path : File::$noFilePng;
    }

    public function fileExist()
    {
        return file_exists($this->getFullPath());
    }

    public function getPrevFile()
    {
        $prevFile = File::find()->where('id = (SELECT max(id) FROM ' . self::tableName() . ' WHERE id < ' . $this->id . ')')->one();
        return $prevFile;
    }

    public function getPrevFileId()
    {
        $prevFile = $this->getPrevFile();
        return isset($prevFile) ? $prevFile->id : null;
    }

    public function getNextFile()
    {
        $nextFile = File::find()->where('id = (SELECT min(id) FROM ' . self::tableName() . ' WHERE id > ' . $this->id . ')')->one();
        return $nextFile;
    }

    public function getNextFileId()
    {
        $nextFile = $this->getNextFile();
        return isset($nextFile) ? $nextFile->id : null;
    }

    public function getFullOriginalNameWithExtension()
    {
        return $this->original_name . '.' . $this->file_extension;
    }
}
