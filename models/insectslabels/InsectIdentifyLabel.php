<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 23.09.2018
 * Time: 20:13
 */

namespace app\models\insectslabels;


class InsectIdentifyLabel
{
    public $det;
    public $date;
    public $firstLineText;
    public $gender;
    public $labelsCount;

    public function __construct(\stdClass $tempClass)
    {
        $this->det = $tempClass->det;
        $this->date = $tempClass->date;
        $this->firstLineText = $tempClass->{'first-line-text'};
        $this->gender = $tempClass->{'gender'};
    }
}
