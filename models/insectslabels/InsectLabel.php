<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 20.01.2019
 * Time: 19:36
 */

namespace app\models\insectslabels;


class InsectLabel
{
    public $geoLabel;
    public $identLabel;

    public function __construct($labelArray)
    {
        $this->geoLabel = new InsectGeoLabel($labelArray[0]);
        $this->identLabel = new InsectIdentifyLabel($labelArray[1]);
    }
}
