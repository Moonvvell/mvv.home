<?php
/**
 * Created by PhpStorm.
 * User: Moonvvell
 * Date: 23.09.2018
 * Time: 17:56
 */

namespace app\models\insectslabels;


class InsectGeoLabel
{
    public $leg;
    public $date;
    public $firstLineText;
    public $secondLineText;
    public $coordinates;
    public $labelsCount;

    public function __construct(\stdClass $tempClass)
    {
        $this->leg = $tempClass->leg;
        $this->date = $tempClass->date;
        $this->firstLineText = $tempClass->{'first-line-text'};
        $this->secondLineText = $tempClass->{'second-line-text'};
        $this->coordinates = $tempClass->{'coordinates'};
    }
}

