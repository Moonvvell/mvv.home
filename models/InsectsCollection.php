<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insects_collection".
 *
 * @property integer $id
 * @property string $catch_date
 * @property string $catch_user_name
 * @property string $catch_country
 * @property string $catch_place
 * @property string $catch_place_description
 * @property string $catch_place_coordinates
 * @property string $definition_date
 * @property string $definition_user_name
 * @property string $latin_name
 * @property string $other_name
 * @property integer $sex
 * @property string $identificator
 * @property string $collection_box
 * @property string $add_date
 * @property string $last_change_date
 */
class InsectsCollection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insects_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catch_date', 'catch_country', 'catch_place', 'catch_place_coordinates', 'catch_user_name', 'catch_place_description', 'identificator', 'collection_box'], 'required'],
            [['catch_date', 'definition_date', 'add_date', 'last_change_date'], 'safe'],
            [['catch_place_description', 'latin_name', 'other_name', 'identificator', 'collection_box'], 'string'],
            [['sex'], 'integer'],
            [['catch_user_name', 'catch_country', 'catch_place', 'definition_user_name'], 'string', 'max' => 255],
            [['catch_place_coordinates'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'catch_date' => Yii::t('app', 'Catch Date'),
            'catch_user_name' => Yii::t('app', 'Catch User Name'),
            'catch_country' => Yii::t('app', 'Catch Country'),
            'catch_place' => Yii::t('app', 'Catch Place'),
            'catch_place_description' => Yii::t('app', 'Catch Place Description'),
            'catch_place_coordinates' => Yii::t('app', 'Catch Place Coordinates'),
            'definition_date' => Yii::t('app', 'Definition Date'),
            'definition_user_name' => Yii::t('app', 'Definition User Name'),
            'latin_name' => Yii::t('app', 'Latin Name'),
            'other_name' => Yii::t('app', 'Other Name'),
            'sex' => Yii::t('app', 'Sex'),
            'identificator' => Yii::t('app', 'Identificator'),
            'collection_box' => Yii::t('app', 'Collection Box'),
            'add_date' => Yii::t('app', 'Add Date'),
            'last_change_date' => Yii::t('app', 'Last Change Date'),
        ];
    }

    /**
     * @throws \RuntimeException
     */

    public function generateId()
    {
        $strength = true;
        $randomBytes = openssl_random_pseudo_bytes(2, $strength);
        if (false === $strength || false === $randomBytes) {
            throw new \RuntimeExcption('IV generation failed');
        }
        $id = date('dM') . '-' . bin2hex($randomBytes);
        $this->identificator = $id;
    }

    /**
     * @return self
     * @throws \RuntimeException
     */
    public function duplicate()
    {
        $new = new self();
        $new->catch_date = $this->catch_date;
        $new->catch_user_name = $this->catch_user_name;
        $new->catch_country = $this->catch_country;
        $new->catch_place = $this->catch_place;
        $new->catch_place_coordinates = $this->catch_place_coordinates;
        $new->catch_place_description = $this->catch_place_description;
        $new->definition_date = $this->definition_date;
        $new->definition_user_name = $this->definition_user_name;
        $new->latin_name = $this->latin_name;
        $new->other_name = $this->other_name;
        $new->sex = $this->sex;
        $new->collection_box = $this->collection_box;
        $new->generateId();
        $new->save();
        return $new;
    }

    /**
     * @return File[]
     */
    public function getFiles()
    {
        $return = array_map(function(InsectsCollectionHasFiles $file){
            return $file->getFile()->one();
        }, InsectsCollectionHasFiles::findAll(['item_id' => $this->id]));
        return $return;
    }
}
